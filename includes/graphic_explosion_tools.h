#ifndef			_GRAPHIC_EXPLOSION_TOOLS_H_
# define		_GRAPHIC_EXPLOSION_TOOLS_H_

# include		"menu_tools.h"
# include		"utils.h"

typedef struct		s_ui_explosion
{
  int			pos_x;
  int			pos_y;
  unsigned int		elapsed_time;
  struct s_ui_explosion	*next;
  struct s_ui_explosion	*prev;
}			t_ui_explosion;

typedef struct		s_ui_explosion_list
{
  t_ui_explosion	*first;
  t_ui_explosion	*last;
  int			element_nb;
}			t_ui_explosion_list;

t_ui_explosion_list	*init_ui_explosion_list();
void			add_ui_explosion_to_list(t_ui_explosion_list*,
						    t_ui_explosion*);
t_bool			init_new_ui_explosion_and_add_it_to_list(t_ui_explosion_list*, int, int);
t_ui_explosion		*my_free_explosion_node(t_ui_explosion_list*, t_ui_explosion*);
void			free_ui_explosion_list(t_ui_explosion_list*);

#endif			/* !_GRAPHIC_EXPLOSION_TOOLS_H_ */
