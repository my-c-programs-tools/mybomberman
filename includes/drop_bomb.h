#ifndef		DROP_BOMB_H_
# define	DROP_BOMB_H_

# include	"game.h"
# include	"player.h"
# include	"utils.h"

t_bool		drop_bomb(t_game*, t_player*, unsigned int);

#endif		/* !DROP_BOMB_H_ */
