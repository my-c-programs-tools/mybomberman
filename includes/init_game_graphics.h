#ifndef			INIT_GAME_GRAPHICS
# define		INIT_GAME_GRAPHICS

# include		<SDL2/SDL.h>
# include		"game_draw.h"
# include		"game.h"
# include		"texture_handler.h"

t_draw_game_stuff	*init_game_graphics(SDL_Renderer*, t_game*);
t_bool			init_textures(SDL_Renderer*, t_texture_pack*);
void			update_dgs(t_game*, t_draw_game_stuff*);

#endif			/* !INIT_GAME_GRAPHICS */
