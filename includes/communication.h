#ifndef		COMMUNICATION_H_
# define        COMMUNICATION_H_

# include	"bonus.h"
# include	"utils.h"

# define	GONE_OBJECT	(8)
# define	RELEASED_BOMB	(9)
# define	DEATH_PLAYER	(10)
# define	WIN_PLAYER	(11)
# define	MOVED_PLAYER	(12)
# define	EXPLOSION	(13)

extern int	g_client_sockets[];

t_bool		send_bonus_to_client(t_bonus*);
t_bool		send_gone_obj_to_client(int, int, int);
t_bool		send_bomb_released_to_client(int, int, int, int);
t_bool		send_win_or_death_to_client(int, int);
t_bool		send_moved_player_to_client(t_player*, int);
t_bool		send_bomb_explosion_to_client(int);
t_bool		send_serialized_game_to_client(int, int, t_game*);
t_bool		send_serialized_game(t_game*);

#endif		/* !COMMUNICATION_H_ */
