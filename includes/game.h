#ifndef		GAME_H_
# define	GAME_H_

# include	"utils.h"
# include	"player.h"
# include	"bomb.h"
# include	"square.h"
# include	"utils.h"

typedef struct	s_game
{
  t_player	*players[4];
  t_bomb	**bombs;
  t_square	***map;
  int		map_width;
  int		map_height;
  int		player_id;
}		t_game;

/*
** game.c
*/
t_game		*init_game(char*, int);
t_bool		game_is_over(t_game*);
t_game		*get_game(t_game*, t_bool);

/*
** free_game.c
*/
void		free_game(t_game*);
void		free_players(t_player**);
void		free_map(t_square***);

/*
** check_map.c
*/
t_bool		check_map(t_game*);

#endif		/* !GAME_H_ */
