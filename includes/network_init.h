#ifndef			_NETWORK_INIT_H_
# define		_NETWORK_INIT_H_

# define		BOMBER_PORT	(8910)
# define		LOCALHOST_IP	("127.0.0.1")

# include		"utils.h"

typedef struct		s_server_info
{
  int			sock_fd;
  int			nb_players;
}			t_server_info;

t_bool			run_server(int port, int nb_players);
int			init_client(char *ip, int port);

#endif			/* !NETWORK_INIT_H_ */
