#ifndef		_FREE_DRAW_GAME_STUFF_H_
# define	_FREE_DRAW_GAME_STUFF_H_

# include	"game_draw.h"
# include	"menu_tools.h"
# include	"utils.h"

void		free_draw_game_stuff(t_draw_game_stuff*);

#endif		/* !_FREE_DRAW_GAME_STUFF_H_ */
