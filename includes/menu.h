#ifndef		_MENU_H_
# define	_MENU_H_

# include	"menu_tools.h"
# include	"utils.h"

t_bool		back_to_main_menu(SDL_Renderer*,
				  t_menu_content*);
t_bool		add_label_to_menu(t_menu_list*, char*,
				  SDL_Renderer*, int);
t_menu_content	*create_menu(SDL_Renderer*);

#endif		/* !_MENU_H_ */
