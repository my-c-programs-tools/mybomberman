#ifndef	       	_SERVER_EVENT_TYPES_H_
# define       	_SERVER_EVENT_TYPES_H_

typedef struct	s_gone_obj_event
{
  int		object_type;
  int		pos_x;
  int		pos_y;
}		t_gone_obj_event;

typedef struct	s_bomb_released_event
{
  int		object_type;
  int		bomb_type;
  int		pos_x;
  int		pos_y;
}		t_bomb_released_event;

typedef struct	s_win_or_death_event
{
  int		object_type;
  int		player_id;
}		t_win_or_death_event;

typedef struct	s_bomb_explosion_event
{
  int		positions_len;
  t_coordinates	*positions;
}		t_bomb_explosion_event;

#endif		/* !_SERVER_EVENT_TYPES_H_ */
