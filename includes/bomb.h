#ifndef			BOMB_H_
# define		BOMB_H_

typedef struct		s_player t_player;

typedef enum		e_bomb_type
  {
    BOMB_CROSS = 0,
    BOMB_DIAGONAL,
    BOMB_STAR,
    BOMB_NUKE
  }			t_bomb_type;

typedef struct		s_bomb
{
  int			x;
  int			y;
  t_bomb_type		type;
  int			range;
  int			elapsed_time;
  t_player		*player;
}			t_bomb;

#endif			/* !BOMB_H_ */
