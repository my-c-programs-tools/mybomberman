#ifndef		SQUARE_H_
# define	SQUARE_H_

#include	"sprite.h"
# include	"utils.h"

typedef enum	e_square_type
  {
    SQUARE_FREE = 0,
    SQUARE_WALL,
    SQUARE_DESTRUCTIBLE,
    SQUARE_BONUS,
    SQUARE_BOMB
  }		t_square_type;

typedef struct	s_square
{
  t_square_type	type;
  void		*object;
  t_sprite	*sprite;
  t_bool	is_exploding;
}		t_square;

#endif		/* !SQUARE_H_ */
