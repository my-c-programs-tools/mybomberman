#ifndef		PARSE_SERVER_FRAME_UTILS_H_
# define	PARSE_SERVER_FRAME_UTILS_H_

#include	"game.h"
#include	"game_draw.h"

t_bool		parse_bonus(t_game*, t_draw_game_stuff*, char*, char);
t_bool		parse_gone_obj(t_game*, t_draw_game_stuff*, char*, char);
t_bool		parse_bomb_released(t_game*, t_draw_game_stuff*, char*, char);
t_bool		parse_win_or_death(t_game*, t_draw_game_stuff*, char*, char);
t_bool		parse_moved_player(t_game*, t_draw_game_stuff*, char*, char);
t_bool		parse_bomb_explosion(t_game*, t_draw_game_stuff*, char*, char);

#endif		/* !PARSE_SERVER_FRAME_UTILS_H_ */
