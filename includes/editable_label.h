#ifndef		_EDITABLE_LABEL_H_
# define	_EDITABLE_LABEL_H_

#include	"menu_tools.h"
#include	"utils.h"

t_bool		update_editable_label(SDL_Renderer*, t_menu_label*);
t_bool		add_ip_field_to_menu(SDL_Renderer*, t_menu_content*, char*);
t_bool		init_editable_menu_fields(SDL_Renderer*, t_menu_content*);

#endif		/* !_EDITABLE_LABEL_H_ */
