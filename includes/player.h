#ifndef			PLAYER_H_
# define		PLAYER_H_

# include		"bomb.h"
# include		"sprite.h"
# include		"utils.h"

typedef struct		s_game t_game;

typedef struct		s_player
{
  double		x;
  double		y;
  int			speed;
  int			bomb_limit;
  int			bomb_range;
  int			used_bombs;
  t_bomb_type		bomb_type;
  t_player_sprite	*sprite;
  int			color;
  t_direction		direction;
  t_bool		is_walking;
}			t_player;

t_player		*init_player(t_game*, int);
t_player		*get_player(t_game*, int, int);

#endif			/* !PLAYER_H_ */
