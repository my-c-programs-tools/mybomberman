#ifndef		USER_MOVEMENT_H_
# define	USER_MOVEMENT_H_

# include	"game.h"
# include	"player.h"
# include	"utils.h"

t_bool		move_left(t_game*, t_player*, unsigned int);
t_bool		move_right(t_game*, t_player*, unsigned int);
t_bool		move_up(t_game*, t_player*, unsigned int);
t_bool		move_down(t_game*, t_player*, unsigned int);

#endif		/* !USER_MOVEMENT_H_ */
