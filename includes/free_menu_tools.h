#ifndef		_FREE_TOOLS_H_
# define	_FREE_TOOLS_H_

# include	"menu_tools.h"

void		clear_menu_labels(t_menu_list*);
void		free_SDL(t_menu_content*);
void		free_list(t_menu_list*);
void		free_menu_content(t_menu_content*);

#endif		/* !_FREE_TOOLS_H_ */
