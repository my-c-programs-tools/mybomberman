#ifndef			_GAME_DRAW_H_
# define		_GAME_DRAW_H_

# include		"graphic_explosion_tools.h"
# include		"menu_tools.h"
# include		"player.h"
# include		"square.h"
# include		"utils.h"

typedef struct		s_draw_game_stuff
{
  SDL_Renderer		*renderer;
  t_square		***map;
  t_ui_explosion_list	*explosion_list;
  t_player		**players;
  int			player_id;
  int			square_width;
  int			square_height;
}			t_draw_game_stuff;

t_bool			draw_game(t_draw_game_stuff*, int, int, int);

#endif			/* !_GAME_DRAW_H_ */
