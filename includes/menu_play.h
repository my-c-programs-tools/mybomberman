#ifndef		_MENU_PLAY_H_
# define	_MENU_PLAY_H_

# include	"menu_tools.h"
# include	"utils.h"

t_bool		init_menu_play_list(SDL_Renderer*, t_menu_list*);
t_bool		init_menu_play(SDL_Renderer*, t_menu_content*);

#endif		/* !_MENU_PLAY_H_ */
