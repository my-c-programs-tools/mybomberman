#ifndef		_END_GAME_DISPLAY_H_
# define	_END_GAME_DISPLAY_H_

# include	"menu_tools.h"
# include	"utils.h"

t_bool		display_end_game(SDL_Renderer*, t_bool);

#endif		/* !_END_GAME_DISPLAY_H_ */
