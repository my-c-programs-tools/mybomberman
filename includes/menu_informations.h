#ifndef		_MENU_INFORMATIONS_H_
# define	_MENU_INFORMATIONS_H_

# include	"menu_tools.h"

SDL_Rect	*get_label_info_position(SDL_Texture*);
t_menu_label	*init_back_information(SDL_Renderer*, char*);

#endif		/* !_MENU_INFORMATIONS_H_ */
