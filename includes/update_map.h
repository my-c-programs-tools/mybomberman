#ifndef		UPDATE_MAP_H_
# define	UPDATE_MAP_H_

# include	"square.h"
# include	"utils.h"

t_bool		create_square_model(char, char*, t_square*);
void		add_object_to_map(t_square***, t_square*, t_coordinates*);

#endif		/* UPDATE_MAP_H_ */
