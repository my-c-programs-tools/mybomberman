#ifndef		_MENU_ANIMATION_H_
# define	_MENU_ANIMATION_H_

# include	"menu_tools.h"

void		init_animation_properties(t_animated_sprite*, int, int);
void		change_flying_bomber_direction(t_animated_sprite*, unsigned int);
void		change_flying_bomber_position(t_animated_sprite*, unsigned int);
void		move_pirate(t_animated_sprite*, unsigned int);
void		animate_pirate(t_animated_sprite*, unsigned int);

#endif		/* !_MENU_ANIMATION_H_ */
