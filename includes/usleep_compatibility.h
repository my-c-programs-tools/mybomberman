#ifndef		USLEEP_COMPATIBILITY_H_
# define	USLEEP_COMPATIBILITY_H_

# ifdef		_WIN32
#  include	<windows.h>
#  define	usleep(micro_sec)   Sleep(micro_sec / 1000)
# endif		/* !_WIN32 */


#endif		/* !USLEEP_COMPATIBILITY_H_ */
