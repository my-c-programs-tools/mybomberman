#ifndef		_MENU_EVENT_H_
# define	_MENU_EVENT_H_

# include	"menu_tools.h"
# include	"utils.h"

t_bool		text_input_event_handler(SDL_Renderer*, t_menu_content*, SDL_Event);
t_bool		change_selected_label(SDL_Renderer*, t_menu_content*,
				      SDL_Keycode);
t_bool		action_event_handler(SDL_Renderer*, t_menu_content*, SDL_Event);

#endif		/* !_MENU_EVENT_H_ */
