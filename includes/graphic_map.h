#ifndef		_GRAPHIC_MAP_H_
# define	_GRAPHIC_MAP_H_

# include	"square.h"
# include	"srcs_path.h"
# include	"game.h"

t_bool		init_map_sprites(SDL_Renderer*, t_game*);

#endif		/* !_GRAPHIC_MAP_H_ */
