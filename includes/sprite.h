#ifndef			_SPRITE_H_
# define		_SPRITE_H_

#include		<SDL2/SDL.h>
#include		<SDL2/SDL_image.h>
#include		"utils.h"

typedef struct		s_animated_sprite
{
  SDL_Texture		*sprite_texture;
  SDL_Rect		sprite_rect;
  SDL_Rect		sprite_renderer_position;
  t_direction		direction;
  t_animation_time	animation_time;
  int			speed;
  int			spe_act;
  t_bool		is_visible;
}			t_animated_sprite;

typedef struct		s_sprite
{
  SDL_Texture		*sprite_texture;
  SDL_Rect		sprite_rect;
}			t_sprite;

typedef struct		s_player_sprite
{
  SDL_Texture		*sprite_texture;
  SDL_Rect		sprite_rect;
  SDL_Rect		sprite_renderer_position;
  t_animation_time	animation_time;
}			t_player_sprite;

#endif			/* !_SPRITE_H_ */
