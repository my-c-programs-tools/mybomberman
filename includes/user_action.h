#ifndef		USER_ACTION_H_
# define	USER_ACTION_H_

# include	"server_loop.h"
# include	"player.h"
# include	"utils.h"

typedef t_bool	(*t_action_fct)(t_game*, t_player*, unsigned int);

typedef struct	s_action
{
  t_action_fct	fct;
  int		input;
}		t_action;

t_bool		user_action(t_player*, int*, int, t_animation_time*);

#endif		/* !USER_ACTION_H_ */
