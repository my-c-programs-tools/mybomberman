#ifndef		_MAP_TEXTURE_HANDLER_H_
# define	_MAP_TEXTURE_HANDLER_H_

# include	"menu_tools.h"
# include	"player.h"
# include	"square.h"
# include	"utils.h"

typedef struct	s_texture_pack
{
  SDL_Texture	*texture;
  char		*path;
}		t_texture_pack;

t_bool		init_textures_tabs(SDL_Renderer*);
void		load_corresponding_texture(t_square*);
void		load_player_color_texture(t_player*);
SDL_Texture	*get_bomb_texture(t_square*);
SDL_Texture	*get_explosion_texture();

#endif		/* !_MAP_TEXTURE_HANDLER_H_ */
