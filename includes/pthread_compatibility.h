#ifndef		PTHREAD_COMPATIBILITY_H_
# define	PTHREAD_COMPATIBILITY_H_

# ifdef		_WIN32
#  define	HAVE_STRUCT_TIMESPEC
# endif		/* _WIN32 */

# include	<pthread.h>

#endif		/* !PTHREAD_COMPATIBILITY_H_ */
