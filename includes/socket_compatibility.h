#ifndef		SOCKET_COMPATIBILITY_H_
# define	SOCKET_COMPATIBILITY_H_

# ifdef		_WIN32

#  include	<Winsock2.h>
#  include	<ws2tcpip.h>
#  define	SOCKET_MODE			1
#  define	SOCKET_CONF(s_fd, mode)		ioctlsocket(s_fd, FIONBIO, &mode)
#  define	SET_SOCK_OPT(opt)		(char*)opt
#  define	SOCK_SEND(fd, buff, size)	send(fd, buff, size, 0)
#  define	SOCK_RECV(fd, buff, size)	recv(fd, buff, size, 0)

# else		/* !_WIN32 */

#  include	<arpa/inet.h>
#  include	<netdb.h>
#  include	<fcntl.h>
#  define	SOCKET_MODE			O_NONBLOCK
#  define	SOCKET_CONF(s_fd, mode)		fcntl(s_fd, F_SETFL, mode)
#  define	SET_SOCK_OPT(opt)		opt
#  define	SOCK_SEND(fd, buff, size)	write(fd, buff, size)
#  define	SOCK_RECV(fd, buff, size)	read(fd, buff, size)

# endif		/* _WIN32 */

#endif		/* !SOCKET_COMPATIBILITY_H_ */
