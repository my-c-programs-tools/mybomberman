#ifndef		_PLAYER_ANIMATION_H_
# define	_PLAYER_ANIMATION_H_

# include	"player.h"

typedef struct	s_textures_position
{
  t_direction	direction;
  int		width;
  int		height;
  int		max_width;
  int		first_sprite_x;
  int		first_sprite_y;
}		t_textures_position;

void		set_player_sprite_properties(t_player*);

#endif		/* !_PLAYER_ANIMATION_H_ */
