#include "utils.h"

#ifndef		STORE_EXPLOSION_COORDS_H_
# define	STORE_EXPLOSION_COORDS_H_

int		nb_explosion_coords();
t_coordinates	*get_explosion_coords(t_coordinates*, t_bool);
t_bool		store_explosion_coords(int, int);
void	        free_explosion_coords();

#endif		/* !STORE_EXPLOSION_COORDS_H_ */
