#ifndef		BONUS_RANDOM_H_
# define	BONUS_RANDOM_H_

#include	"bonus.h"
#include	"player.h"

void		init_bomb_percentages();
void		bonus_brandom(t_player*, t_bonus*);

#endif		/* !BONUS_RANDOM_H_ */
