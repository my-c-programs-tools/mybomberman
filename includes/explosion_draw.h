#ifndef		_EXPLOSION_DRAW_H_
# define	_EXPLOSION_DRAW_H_

# include	"game_draw.h"
# include	"menu_tools.h"
# include	"utils.h"

t_bool		draw_game_explosions(t_draw_game_stuff*, int, int, int);

#endif		/* !_EXPLOSION_DRAW_H_ */
