#ifndef			_GAME_LOOP_H_
# define		_GAME_LOOP_H_

# include		<SDL2/SDL.h>
# include		"utils.h"

typedef struct		s_key_scan
{
  Uint8			scan_code;
  int			key_code;
}			t_key_scan;

t_bool			game_loop(SDL_Renderer*, int);

#endif			/* !_GAME_LOOP_H_ */
