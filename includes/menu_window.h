#ifndef		_MENU_WINDOW_H_
# define	_MENU_WINDOW_H_

# include	"menu.h"
# include	"utils.h"

t_bool		init_window();
void		init_animated_sprites_properties(t_menu_content*);
void		menu_event_loop(SDL_Renderer*, t_menu_content*);
void		free_SDL(t_menu_content*);

#endif		/* !_MENU_WINDOW_H_ */
