#ifndef		_BONUS_PLAYER_H_
# define	_BONUS_PLAYER_H_

# include	"player.h"
# include	"bonus.h"

void		bonus_speed(t_player*, t_bonus*);
void		bonus_range(t_player*, t_bonus*);
void		bonus_bomb(t_player*, t_bonus*);

#endif	        /* !BONUS_PLAYER_H_ */
