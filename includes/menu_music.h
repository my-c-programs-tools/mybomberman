#ifndef		_MENU_MUSIC_H_
# define	_MENU_MUSIC_H_

# include	"menu_tools.h"
# include	"utils.h"

typedef struct	s_music_path
{
  char		*label_name;
  char		*music_path;
}		t_music_path;

t_bool		init_menu_list_music(SDL_Renderer*, t_menu_list*);
t_bool		init_menu_music(SDL_Renderer*, t_menu_content*);
t_bool		change_soundtrack(t_menu_content*, char*);
t_bool		select_soundtrack(t_menu_content*);


#endif		/* !_MENU_MUSIC_H_ */
