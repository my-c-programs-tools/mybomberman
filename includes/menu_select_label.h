#ifndef		_MENU_SELECT_LABEL_H_
# define	_MENU_SELECT_LABEL_H_

# include	"menu_tools.h"
# include	"utils.h"

t_bool		select_label(SDL_Renderer*, t_menu_content*);

#endif		/* !MENU_SELECT_LABEL_H_ */
