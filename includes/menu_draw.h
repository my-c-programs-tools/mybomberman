#ifndef		_MENU_DRAW_H_
# define	_MENU_DRAW_H_

# include	"menu_tools.h"

t_bool		draw_menu(SDL_Renderer*, t_menu_content*, unsigned int);

#endif		/* !_MENU_DRAW_H_ */
