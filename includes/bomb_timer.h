#ifndef		BOMB_TIMER_H_
# define	BOMB_TIMER_H_

# include	"bomb.h"
# include	"game.h"
# include	"utils.h"

t_bool		refresh_bombs(unsigned int);
t_bool		remote_trigger(t_bomb*);

#endif		/* !BOMB_TIMER_H_ */
