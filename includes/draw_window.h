#ifndef		_DRAW_WINDOW_H_
# define	_DRAW_WINDOW_H_

# include	"menu_tools.h"
# include	"utils.h"

t_bool		draw_window(SDL_Renderer*, t_menu_content*, unsigned int);

#endif		/* !_DRAW_WINDOW_H_ */
