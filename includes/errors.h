#ifndef		ERRORS_H_
# define	ERRORS_H_

# include	"utils.h"

/*
** Messages
*/

# define        MEMORY_ERROR		"[FATAL ERROR] Out of memory\n"
# define	READ_ERROR		"[FATAL ERROR] Read error\n"
# define	OPEN_ERROR		"[FATAL ERROR] Couldn't open file\n"
# define	CLOSE_ERROR		"[FATAL ERROR] Couldn't close file\n"
# define	SDL_INIT_ERROR		"[FATAL ERROR] SDL init failed\n"
# define	SDL_WIN_ERROR		"[FATAL ERROR] SDL window creation failed\n"
# define	SDL_RENDER_CREATE_ERROR	"[FATAL ERROR] SDL Render create error\n"
# define	SDL_RENDER_CLEAR_ERROR	"[FATAL ERROR] SDL Render clear error\n"
# define	TEXTURE_QUERY_ERROR	"[FATAL ERROR] SDL texture query error\n"
# define	TEXTURE_ERROR		"[FATAL ERROR] Texture creation failed\n"
# define	RENDER_TEXT_ERROR	"[FATAL_ERROR] Couldn't create a render text\n"
# define	LOAD_MUSIC_ERROR	"[FATAL ERROR] Couldn't load music\n"
# define	MUSIC_PLAY_ERROR	"[FATAL ERROR] Couldn't play music\n"
# define        THREAD_ERROR	        "[FATAL ERROR] Couldn't start thread\n"
# define	WSA_STRT_ERROR		"[FATAL ERROR] WSASTARTUP error\n"
# define	SCKET_ERROR		"[FATAL ERROR] Socket error\n"
# define	SCKT_CNF_ERROR		"[FATAL ERROR] Socket configuration error\n"
# define	MAP_ERROR		"[FATAL ERROR] Map has wrong format\n"
# define	MESSAGEBOX_ERROR	"[FATAL ERROR] MessageBox creation failed\n"

/*
** Functions
*/

void		write_error(char*);
t_bool		my_bool_error(char*);
int		my_int_error(char*, int);
void		*my_null_error(char*);
t_state		my_state_error(char*);

#endif		/* !ERRORS_H_ */
