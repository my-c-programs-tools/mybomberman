#ifndef	       	_SERVER_LOOP_
# define       	_SERVER_LOOP_

# include      	"game.h"
# include      	"utils.h"

typedef struct	s_user_input
{
  unsigned int  magic;
  int           input;
}				t_user_input;

t_bool	       	server_loop(int nb_players, t_game *game);

#endif		/* !_SERVER_LOOP_ */
