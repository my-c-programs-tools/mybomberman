#ifndef		_MENU_FLYING_BOMBER_H_
# define	_MENU_FLYING_BOMBER_H_

# include	"menu_tools.h"
# include	"utils.h"

t_bool		init_flying_bomber_bomb(t_menu_content *menu_content);
void		handle_explode_animation(t_menu_content *menu_content);
void		start_explode_animation(t_menu_content *menu_content,
						unsigned int elapsed);
void		menu_drop_bomb(t_menu_content*, unsigned int);
t_bool		should_drop_bomb(t_menu_content *menu_content,
				 unsigned int elapsed);


#endif		/* !_MENU_FLYING_BOMBER_H_ */
