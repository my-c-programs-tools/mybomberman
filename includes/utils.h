/*
** utils.h for etna in /home/bakhou_r/ETNA/DEVC/Bomberman/includes
**
** Made by BAKHOUCHE Raphaël
** Login   <bakhou_a@etna-alternance.net>
**
** Started on  Thu May 18 11:51:46 2017 BAKHOUCHE Raphaël
** Last update Fri Oct 25 08:55:31 2019 VEROVE Jordan
*/

#ifndef			UTILS_H_
# define		UTILS_H_

# define		UNUSED(a)	(void)(a)

# define		ABS(nb)		((nb < 0) ? (-nb) : (nb))

# define		RGT_AXIS	{  1,  0 }
# define		LFT_AXIS	{ -1,  0 }
# define		UP_AXIS		{  0, -1 }
# define		DWN_AXIS	{  0,  1 }
# define		UP_LFT_AXIS	{ -1, -1 }
# define		UP_RGT_AXIS	{  1, -1 }
# define		DWN_LFT_AXIS	{ -1,  1 }
# define		DWN_RGT_AXIS	{  1,  1 }
# define		NULL_AXIS	{  0,  0 }

# define		MAX_SOCKET_LEN	512

/*
** MinGW's gcc definitly don't like that TRUE/FALSE enum :'(
*/
# ifdef			_WIN32
#  define		TRUE		1
#  define		FALSE		0
typedef			int		t_bool;
# else
typedef enum		e_bool
  {
    FALSE = 0,
    TRUE
  }			t_bool;
#endif

typedef enum		e_state
  {
    FAILURE,
    SUCCESS,
    NEUTRAL
  }			t_state;

typedef enum		e_direction
  {
    DIRECTION_UP = 0,
    DIRECTION_RIGHT,
    DIRECTION_DOWN,
    DIRECTION_LEFT
  }			t_direction;

typedef struct		s_coordinates
{
  int			x;
  int			y;
}			t_coordinates;

typedef struct		s_animation_time
{
  unsigned int		last_update;
  unsigned int		refresh_time;
}			t_animation_time;


/*
** Axes and coordinates are two different logical
** elements even if structurally identical
*/
typedef struct		s_axis
{
  int			x;
  int			y;
}			t_axis;

/*
** closest.c
*/
int			closest(int, int, int);

/*
** my_strcmp.c
*/
int			my_strcmp(char*, char*);

/*
** my_strlen.c
*/
int			my_strlen(char*);

/*
** my_strcat.c
*/
char			*my_strcat(char*, char*);

/*
** my_strcpy.c
*/
char			*my_strcpy(char*, char*);

/*
** my_strncpy.c
*/
char			*my_strncpy(char*, char*, int);

/*
** my_remove_n_first_char.c
*/
void			my_remove_n_first_char(char*, int);

/*
** my_getnbr.c
*/
int			my_getnbr(char*);

#endif			/* !UTILS_H_ */
