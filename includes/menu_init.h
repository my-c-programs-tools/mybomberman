#ifndef		_MENU_INIT_H_
# define	_MENU_INIT_H_

# include	"menu_tools.h"
# include	"utils.h"

t_menu_list	*init_menu_list();
void		init_animations_rect();
t_bool		init_menu_animation();
t_bool		init_menu_properties(SDL_Renderer*, t_menu_content*);
t_menu_content	*init_menu_content(SDL_Renderer*, t_menu_list*);

#endif		/* !MENU_INIT_H_ */
