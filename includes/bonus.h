#ifndef		BONUS_H_
# define	BONUS_H_

# include	"utils.h"
# include	"player.h"
# include	"game.h"

/*
**  BONUS_SPEED     -> Player's speed
**  BONUS_RANGE     -> Player's bomb range
**  BONUS_BOMB      -> Player's limit bomb
**  BONUS_BCROSS    -> Set Player's bomb to BOMB_CROSS
**  BONUS_BDIAGONAL -> Set Player's bomb to BOMB_DIAGONAL
**  BONUS_BSTAR     -> Set Player's bomb to BOMB_STAR
**  BONUS_BNUKE     -> Set Player's bomb to BOMB_NUKE
**  BONUS_BRANDOM   -> Set Player's bomb to random type
*/
typedef enum	e_bonus_type
  {
    BONUS_SPEED,
    BONUS_RANGE,
    BONUS_BOMB,
    BONUS_BCROSS,
    BONUS_BDIAGONAL,
    BONUS_BSTAR,
    BONUS_BNUKE,
    BONUS_BRANDOM
  }		t_bonus_type;

typedef struct	s_bonus
{
  int		x;
  int		y;
  t_bonus_type	type;
  t_bool	malus;
}		t_bonus;

typedef struct	s_bonus_stat
{
  t_bonus_type	type;
  int		value;
  double        prct;
  void		(*bonus_function)(t_player*, t_bonus*);
}		t_bonus_stat;

void		init_bonus_percentages();
t_bool		apply_bonus(t_game*, t_player*, t_bonus*);
t_bonus		*spawn_bonus(int, int);

#endif		/* !BONUS_H_ */
