#ifndef	       	SEND_TO_CLIENT_H_
# define       	SEND_TO_CLIENT_H_

#include	"game.h"
#include	"player.h"
#include	"bonus.h"

extern int g_client_sockets[];

t_bool		send_event_to_client(int, char*);

#endif		/* !SEND_TO_CLIENT_H_ */
