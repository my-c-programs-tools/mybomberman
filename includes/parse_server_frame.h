#ifndef		PARSE_SERVER_FRAME_H_
# define	PARSE_SERVER_FRAME_H_

#include	"game.h"
#include	"game_draw.h"

typedef t_bool	(*t_recv_event_action)(t_game*, t_draw_game_stuff*, char*, char);

t_bool		parse_server_frame(t_game*, t_draw_game_stuff*, char*);

#endif		/* !PARSE_SERVER_FRAME_H_ */
