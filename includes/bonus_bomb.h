#ifndef		_BONUS_BOMB_H_
# define	_BONUS_BOMB_H_

# include	"bonus.h"
# include	"player.h"

void		bonus_bnuke(t_player*, t_bonus*);
void		bonus_bcross(t_player*, t_bonus*);
void		bonus_bstar(t_player*, t_bonus*);
void		bonus_bdiagonal(t_player*, t_bonus*);

#endif	        /* !BONUS_BOMB_H_ */
