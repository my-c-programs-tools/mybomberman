#ifndef		MESSAGE_H_
# define	MESSAGE_H_

# define	MESSAGE_BACK		"press escape to return main menu"
# define	TITLE_ERROR		"ERROR"
# define	IP_ERROR		"Bad IP adress selected"
# define	NB_PLAYER_ERROR		"Bad players number selected\nMust be between 2 and 4"
# define	LABEL_CREATE_GAME	"CREATE GAME : "
# define	LABEL_JOIN_GAME		"JOIN GAME : "
# define	LABEL_PLAY		"PLAY"
# define	LABEL_MUSIC		"MUSIC"
# define	LABEL_QUIT		"QUIT"
# define	DEFAULT_IP		" IP"
# define	DEFAULT_NB_PLAYER	" NB PLAYER"
# define	GAME_WON		"GAME WON !! GGWP DUDE"
# define	GAME_LOST		"GAME LOST... MAYBE HAVE A BREAK AND TRY AGAIN LATER ? :P"

#endif		/* !MESSAGE_H_ */
