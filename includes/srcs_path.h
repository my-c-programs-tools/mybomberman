#ifndef		SRCS_PATH_H_
# define    SRCS_PATH_H_

# ifdef _WIN32

# define	RSCR_PATH			            ""

# else

#  define	RSCR_PATH			            "/usr/share/bomberman/"

# endif

# define	MENU_FONT_PATH			        RSCR_PATH "fonts/Munro.ttf"
# define	MENU_INFO_FONT_PATH		        RSCR_PATH "fonts/MobileFont.ttf"
# define	FLYING_BOMBER_PATH		        RSCR_PATH "images/flying_bomber.png"
# define	MENU_PIRATE_PATH		        RSCR_PATH "images/menu_pirate.png"
# define	MENU_BACKGROUND_PATH		    RSCR_PATH "images/background_menu.png"
# define	SELECTED_LABEL_LOGO_PATH	    RSCR_PATH "images/selected_label_logo.png"
# define	MENU_LOGO_PATH			        RSCR_PATH "images/bomberman_logo.png"
# define	BOMB_PATH			            RSCR_PATH "images/bomb.png"
# define	WALL_TEXTURE_PATH		        RSCR_PATH "images/wall.png"
# define	FREE_TEXTURE_PATH		        RSCR_PATH "images/floor.png"
# define	DESTRUCTIBLE_TEXTURE_PATH	    RSCR_PATH "images/wood.png"
# define	EXPLOSION_TEXTURE_PATH		    RSCR_PATH "images/bomb_explosion1.png"
# define	B_SPEED_TEXTURE_PATH		    RSCR_PATH "images/bonus_player_speed.png"
# define	B_RANGE_TEXTURE_PATH		    RSCR_PATH "images/bonus_range_bomb.png"
# define	B_BOMB_TEXTURE_PATH		        RSCR_PATH "images/bonus_more_bomb.png"
# define	B_BOMB_CROSS_TEXTURE_PATH	    RSCR_PATH "images/bonus_cross_bomb.png"
# define	B_BOMB_DIAGONAL_TEXTURE_PATH	RSCR_PATH "images/bonus_diagonal_bomb.png"
# define	B_BOMB_STAR_TEXTURE_PATH	    RSCR_PATH "images/bonus_star_bomb.png"
# define	B_BOMB_NUKE_TEXTURE_PATH	    RSCR_PATH "images/bonus_nuke_bomb.png"
# define	B_BOMB_RANDOM_TEXTURE_PATH	    RSCR_PATH "images/bonus_random_bomb.png"
# define	P_DARK_TEXTURE_PATH		        RSCR_PATH "images/bomberman_black.png"
# define	P_GRAY_BLUE_TEXTURE_PATH	    RSCR_PATH "images/bomberman_gray_blue.png"
# define	P_ORANGE_TEXTURE_PATH		    RSCR_PATH "images/bomberman_orange.png"
# define	P_PINK_TEXTURE_PATH		        RSCR_PATH "images/bomberman_pink.png"
# define	B_CROSS_TEXTURE_PATH		    RSCR_PATH "images/bomb_anim.png"
# define	B_DIAGONAL_TEXTURE_PATH		    RSCR_PATH "images/bomb_diagonal.png"
# define	B_STAR_TEXTURE_PATH		        RSCR_PATH "images/bomb_star.png"
# define	B_NUKE_TEXTURE_PATH		        RSCR_PATH "images/bomb_nuke.png"
# define	MAP_PATH			            RSCR_PATH "maps/classic.map"
# define	NERVOUS_BOMBER_MUSIC_PATH	    RSCR_PATH "sounds/nervous_bomberman.wav"
# define	GASOLINA_MUSIC_PATH		        RSCR_PATH "sounds/gasolina.wav"
# define	SLAVES_OF_FIRE_MUSIC_PATH	    RSCR_PATH "sounds/slaves_of_fire.wav"
# define	SARIAS_SONG_MUSIC_PATH		    RSCR_PATH "sounds/ocarina_of_time_sarias_song.wav"
# define	LINK_TO_THE_PAST_MUSIC_PATH	    RSCR_PATH "sounds/link_to_the_past.wav"

#endif		/* !SRCS_PATH_H_ */
