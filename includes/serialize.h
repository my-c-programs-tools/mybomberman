#ifndef	    SERIALIZE_H_
# define    SERIALIZE_H_

#include	"game.h"
#include	"player.h"
#include	"bonus.h"
#include	"server_event_types.h"
#include	"utils.h"

/*
** serialize_utils.c
*/
char		*serialize_char(char*, char);
char		*serialize_int(char*, int);
char		*serialize_double(char*, double);
char		*deserialize_char(char*, char*);
char		*deserialize_int(char*, int*);
char		*deserialize_double(char*, double*);

/*
** serialize_game.c
*/
int		    count_players(t_player**);
char		*deserialize_players(char*, t_game*);
char		*deserialize_map(char*, t_game*);
char		*serialize_game(char*, t_game*);
t_game		*deserialize_game(char*);

/*
** serialize_map_elements.c
*/
char		*serialize_bomb(char*, t_bomb*);
char		*deserialize_bomb(char*, t_bomb*);
char		*serialize_bonus(char*, t_bonus*);
char		*deserialize_bonus(char*, t_bonus*);
char		*serialize_square(char*, t_square*);
char		*deserialize_square(char*, t_square*);

/*
** player.c
*/
char		*serialize_player(char*, t_player*);
char		*deserialize_player(char*, t_player*);

/*
** serialize_server_event.c
*/
char		*serialize_bonus_event(char*, t_bonus*);
char		*serialize_gone_obj_event(char*, int, int, int);
char		*serialize_bomb_released_event(char*, int, int, int, int);
char		*serialize_win_or_death_event(char*, int, int);
char		*serialize_moved_player_event(char*, t_player*, int);
char		*serialize_bomb_explosion_event(char*, int);

/*
** deserialize_server_event.c
*/
char		*deserialize_bonus_event(char*, t_bonus*, char);
char		*deserialize_gone_obj_event(char*, t_gone_obj_event*, char);
char		*deserialize_bomb_released_event(char*, t_bomb_released_event*, char);
char		*deserialize_win_or_death_event(char*, t_win_or_death_event*, char);
char		*deserialize_moved_player_event(char*, t_player*);
char		*deserialize_bomb_explosion_event(char*, t_bomb_explosion_event*);

#endif		/* !SERIALIZE_H_ */
