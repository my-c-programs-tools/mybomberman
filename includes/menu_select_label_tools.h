#ifndef		_MENU_SELECT_LABEL_TOOLS_H_
# define	_MENU_SELECT_LABEL_TOOLS_H_

# include	"menu_tools.h"

int		get_nb_player_selected(t_menu_content*);
char		*get_ip_selected(t_menu_content*);
t_bool		game_launching_error(char*);

#endif		/* !_MENU_SELECT_LABEL_TOOLS_H_ */
