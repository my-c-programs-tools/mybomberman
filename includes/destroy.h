#ifndef		DESTROY_H_
# define	DESTROY_H_

# include	"game.h"
# include	"utils.h"

typedef t_state	(*t_destroy_fct)(t_game*, int, int);

/*
** destroy.c
*/
t_bool		destroy_players();
t_state		destroy(int, int);

/*
** destroy_functions.c
*/
t_state		ignore_explosion(t_game*, int, int);
t_state		destroy_player(t_game*, int, int);
t_state		destroy_bonus(t_game*, int, int);
t_state		destroy_bomb(t_game*, int, int);
t_state		destroy_wall(t_game*, int, int);

#endif		/* !DESTROY_H_ */
