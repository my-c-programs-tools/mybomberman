#ifndef			_MENU_TOOLS_H_
# define		_MENU_TOOLS_H_

# include		<SDL2/SDL_ttf.h>
# include		<SDL2/SDL_mixer.h>
# include		"utils.h"
# include		"sprite.h"

typedef struct		s_menu_label
{
  TTF_Font		*label_font;
  SDL_Color		label_color;
  SDL_Surface		*label_surface;
  SDL_Texture		*label_texture;
  SDL_Rect		label_position;
  char			*label_text;
  struct s_menu_label	*next;
  struct s_menu_label	*prev;
}			t_menu_label;

typedef struct		s_menu_list
{
  t_menu_label		*first;
  t_menu_label		*last;
  int			element_nb;
}			t_menu_list;

typedef struct		s_menu_content
{
  t_menu_list		*menu_list;
  t_menu_label		*selected_label;
  t_menu_label		*back_information;
  t_menu_label		*ip_field;
  t_menu_label		*nb_player_field;
  t_sprite		*menu_background;
  t_sprite		*menu_logo;
  t_sprite		*selected_label_logo;
  t_animated_sprite	*menu_flying_bomber;
  t_animated_sprite	*menu_pirate;
  t_animated_sprite	*bomb;
  t_coordinates		*bomb_destination;
  t_bool		is_already_bomb;
  t_bool		is_in_game;
  t_bool		is_in_main_menu;
  t_bool		is_in_play_menu;
  t_bool		is_ip_field_selected;
  t_bool		is_nb_player_selected;
  SDL_Rect		menu_logo_position;
  Mix_Music		*menu_music;
}			t_menu_content;

t_menu_label		*create_new_label(SDL_Color*, SDL_Renderer*,
					  char*, int);
void			add_label_to_menu_list(t_menu_list*, t_menu_label*);
SDL_Rect		*get_logo_position(SDL_Texture*);
SDL_Rect		*get_label_position(SDL_Texture*, int);
t_bool			update_label(SDL_Renderer*, t_menu_label*);

#endif			/* !_MENU_TOOLS_H_ */
