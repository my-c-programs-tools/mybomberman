#ifndef		TIMER_H_
# define	TIMER_H_

# define	CLT_TIMER_ID	(0)
# define	SRV_TIMER_ID	(1)

# define	NB_TIMERS	(2)

void		init_timer(int);
unsigned int	update_timer(int);

#endif		/* !TIMER_H_ */
