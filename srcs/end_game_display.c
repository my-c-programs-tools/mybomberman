#include <stdlib.h>
#include "configuration.h"
#include "errors.h"
#include "end_game_display.h"
#include "messages.h"
#include "srcs_path.h"

SDL_Rect	*get_end_label_position(SDL_Texture *label_texture)
{
  SDL_Rect	*end_label_position;
  int		width;
  int		height;

  if ((end_label_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  SDL_QueryTexture(label_texture, NULL, NULL, &width, &height);
  end_label_position->x = (WINDOW_WIDTH / 2) - (width / 2);
  end_label_position->y = (WINDOW_HEIGHT / 2) - (height / 2);
  end_label_position->w = width;
  end_label_position->h = height;
  return (end_label_position);
}

t_menu_label	*init_end_message(SDL_Renderer *renderer, SDL_Color *color,
				  char *label_text)
{
  t_menu_label	*new_label;
  TTF_Font	*label_font;
  SDL_Surface	*label_surface;
  SDL_Rect	*label_position;

  if ((new_label = malloc(sizeof(t_menu_label))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  if ((label_font = TTF_OpenFont(MENU_FONT_PATH, 30)) == NULL)
    return (my_null_error(OPEN_ERROR));
  if ((label_surface =
       TTF_RenderText_Solid(label_font, label_text, *color)) == NULL)
    return (my_null_error(RENDER_TEXT_ERROR));
  new_label->label_color = *color;
  new_label->label_text = label_text;
  new_label->label_font = label_font;
  new_label->label_surface = label_surface;
  if ((new_label->label_texture =
       SDL_CreateTextureFromSurface(renderer, label_surface)) == NULL)
    return (my_null_error(TEXTURE_ERROR));
  if ((label_position = get_end_label_position(new_label->label_texture)) == NULL)
    return (NULL);
  new_label->label_position = *label_position;
  return (new_label);
}

t_bool		check_user_input()
{
  SDL_Event e;

  if (SDL_PollEvent(&e))
    {
      if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_ESCAPE ||
				  e.key.keysym.sym == SDLK_RETURN ||
				  e.key.keysym.sym == SDLK_SPACE))
	return (FALSE);
    }
  return (TRUE);
}

t_bool		display_end_message(SDL_Renderer *renderer, t_menu_label *end_message)
{
  if (SDL_RenderCopy(renderer, end_message->label_texture,
		     NULL, &end_message->label_position) != 0)
    return (FALSE);
  return (TRUE);
}

t_bool		display_end_game(SDL_Renderer *renderer, t_bool is_a_win)
{
  t_menu_label	*end_message;
  char		run;
  
  run = 0;
  SDL_Color win_color = {50, 204, 151, 0};
  SDL_Color loose_color = {192, 14, 14, 0};
  if (is_a_win)
    end_message = init_end_message(renderer, &win_color, GAME_WON);
  else
    end_message = init_end_message(renderer, &loose_color, GAME_LOST);
  while (run == 0)
    {
      SDL_RenderClear(renderer);
      if (check_user_input() == FALSE)
	run = 1;
      if (display_end_message(renderer, end_message) == FALSE)
	return (FALSE);
      SDL_RenderPresent(renderer);
    }
  return (TRUE);
}
