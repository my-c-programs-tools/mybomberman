#include "utils.h"

void	my_remove_n_first_char(char *s, int n)
{
  char	*s2;

  s2 = s + n;
  while (*s2)
    {
      *s = *s2;
      ++s;
      ++s2;
    }
  *s = '\0';
}
