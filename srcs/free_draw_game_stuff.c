#include <stdlib.h>
#include "free_draw_game_stuff.h"
#include "game.h"

void	free_draw_game_stuff(t_draw_game_stuff *dgs)
{
  int	i;

  free_map(dgs->map);
  free_ui_explosion_list(dgs->explosion_list);
  i = 0;
  while (i < 4)
    {
      free(dgs->players[i]);
      i++;
    }
  free(dgs);
}
