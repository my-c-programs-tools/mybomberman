#include "free_menu_tools.h"
#include "configuration.h"
#include "errors.h"
#include "menu.h"
#include "menu_event.h"
#include "menu_init.h"
#include "menu_music.h"
#include "srcs_path.h"

t_music_path music_paths[] =
  {
    {"NERVOUS BOMERBMAN", NERVOUS_BOMBER_MUSIC_PATH},
    {"GASOLINA", GASOLINA_MUSIC_PATH},
    {"SLAVES OF FIRE", SLAVES_OF_FIRE_MUSIC_PATH},
    {"SARIAS SONG", SARIAS_SONG_MUSIC_PATH},
    {"LINK TO THE PAST", LINK_TO_THE_PAST_MUSIC_PATH},
    {NULL, NULL}
  };

t_bool	change_soundtrack(t_menu_content *menu_content, char *soundtrack)
{
  int	i;

  i = 0;
  while (music_paths[i].label_name != NULL)
    {
      if (my_strcmp(soundtrack, music_paths[i].label_name) == 0)
	{
	  if ((Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)) == -1 ||
	      !(menu_content->menu_music = Mix_LoadMUS(music_paths[i].music_path)) ||
	      (Mix_PlayMusic(menu_content->menu_music, 1) == -1))
	    return (my_bool_error(LOAD_MUSIC_ERROR));
	  return (TRUE);
	}
      i++;
    }
  return (TRUE);
}

t_bool	select_soundtrack(t_menu_content *menu_content)
{
  int	i;

  i = 0;
  while (music_paths[i].label_name != NULL)
    {
      if (my_strcmp(menu_content->selected_label->label_text,
		    music_paths[i].label_name) == 0)
	{
	  if (change_soundtrack(menu_content,
				menu_content->selected_label->label_text) == FALSE)
	    return (FALSE);
	  return (TRUE);
	}
      i++;
    }
  return (TRUE);
}

t_bool	init_menu_music_list(SDL_Renderer *renderer, t_menu_list *menu_list)
{
  int	i;

  i = 0;
  while (music_paths[i].label_name != NULL)
    {
      if (add_label_to_menu(menu_list, music_paths[i].label_name, renderer, i) != TRUE)
	return (my_bool_error(MEMORY_ERROR));
      i++;
    }
  return (TRUE);
}

t_bool	init_menu_music(SDL_Renderer *renderer, t_menu_content *menu_content)
{
  clear_menu_labels(menu_content->menu_list);
  if ((menu_content->menu_list = init_menu_list()) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if (init_menu_music_list(renderer, menu_content->menu_list) == FALSE)
    return (my_bool_error(MEMORY_ERROR));
  menu_content->selected_label = menu_content->menu_list->first;
  if (change_selected_label(renderer, menu_content, 0) == FALSE)
    return (FALSE);
  menu_content->is_in_main_menu = FALSE;
  return (TRUE);
}
