#include <string.h>
#include "editable_label.h"
#include "errors.h"
#include "menu_event.h"
#include "menu_free.h"
#include "menu_select_label.h"
#include "menu.h"

void	text_input_deletion(SDL_Renderer *renderer, t_menu_content *menu_content)
{
  if (menu_content->is_ip_field_selected == TRUE)
    {
      if (my_strcmp(menu_content->ip_field->label_text, " IP") == 0)
	my_strcpy(menu_content->ip_field->label_text, " ");
      if (my_strlen(menu_content->ip_field->label_text) > 1)
	menu_content->ip_field->label_text =
	  my_strncpy(menu_content->ip_field->label_text,
		     menu_content->ip_field->label_text,
		     my_strlen(menu_content->ip_field->label_text) - 1);
      update_editable_label(renderer, menu_content->ip_field);
    }
  else if (menu_content->is_nb_player_selected == TRUE)
    {
      if (my_strcmp(menu_content->nb_player_field->label_text, " NB PLAYER") == 0)
	my_strcpy(menu_content->nb_player_field->label_text, " ");
      if (my_strlen(menu_content->nb_player_field->label_text) > 1)
	menu_content->nb_player_field->label_text =
	  my_strncpy(menu_content->nb_player_field->label_text,
		     menu_content->nb_player_field->label_text,
		     my_strlen(menu_content->nb_player_field->label_text) - 1);
      update_editable_label(renderer, menu_content->nb_player_field); 
    }
}

t_bool	text_input_addition(SDL_Renderer *renderer, t_menu_content *menu_content,
			    SDL_Event e)
{
  if (menu_content->is_ip_field_selected == TRUE)
    {
      if (my_strcmp(menu_content->ip_field->label_text, " IP") == 0)
	my_strcpy(menu_content->ip_field->label_text, " ");
      menu_content->ip_field->label_text =
	strcat(menu_content->ip_field->label_text, e.text.text);
      update_editable_label(renderer, menu_content->ip_field);
    }
  else if (menu_content->is_nb_player_selected == TRUE)
    {
      if (my_strcmp(menu_content->nb_player_field->label_text, " NB PLAYER") == 0)
	my_strcpy(menu_content->nb_player_field->label_text, " ");
      menu_content->nb_player_field->label_text =
	strcat(menu_content->nb_player_field->label_text, e.text.text);
      update_editable_label(renderer, menu_content->nb_player_field);
    }
  return (TRUE);
}

t_bool	text_input_event_handler(SDL_Renderer *renderer,
				   t_menu_content *menu_content, SDL_Event e)
{
  if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_ESCAPE)
    {
      if (back_to_main_menu(renderer, menu_content) == FALSE)
	return (FALSE);
    }
  else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_BACKSPACE)
    text_input_deletion(renderer, menu_content);
  else if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_UP ||
			      e.key.keysym.sym == SDLK_DOWN))
    change_selected_label(renderer, menu_content, e.key.keysym.sym);
  else if (e.type == SDL_TEXTINPUT)
    {
      if (text_input_addition(renderer, menu_content, e) == FALSE)
	return (FALSE);
    }
  else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_RETURN)
    {
      if (select_label(renderer, menu_content)
	  == FALSE)
	return (FALSE);
    }
  return (TRUE);
}

t_bool	following_event_handler(SDL_Renderer *renderer,
				t_menu_content *menu_content, SDL_Event e)
{
  if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_RETURN ||
			      e.key.keysym.sym == SDLK_SPACE))
    {
      if (select_label(renderer, menu_content)
	  == FALSE)
	return (FALSE);
    }
  else if (menu_content->is_in_main_menu == FALSE && e.type == SDL_KEYUP
	   && e.key.keysym.sym == SDLK_ESCAPE)
    {
      if (back_to_main_menu(renderer, menu_content) == FALSE)
	return (FALSE);
    }
  return (TRUE);
}

t_bool	action_event_handler(SDL_Renderer *renderer, t_menu_content *menu_content,
		      SDL_Event e)
{
  if (e.type == SDL_QUIT)
    return (FALSE);
  else if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_UP ||
				   e.key.keysym.sym == SDLK_DOWN))
    {
      if (change_selected_label(renderer, menu_content, e.key.keysym.sym)
	  == FALSE)
	return (FALSE);
    }
  else if (following_event_handler(renderer, menu_content, e) == FALSE)
    return (FALSE);
  return (TRUE);
}
