#include <stdlib.h>
#include "destroy.h"
#include "store_explosion_coords.h"

t_destroy_fct	g_destroy_elem[] = {
  ignore_explosion,
  ignore_explosion,
  destroy_wall,
  destroy_bonus,
  destroy_bomb
};

t_bool		destroy_players()
{
  t_game	*game;
  int		i;

  i = 0;
  game = get_game(NULL, FALSE);
  while (i < 4)
    {
      if (game->players[i])
	free(game->players[i]);
      game->players[i] = NULL;
      ++i;
    }
  return (TRUE);
}

/*
** FAILURE : Critical error
** SUCCESS : Touched something
** NEUTRAL : Touched nothing or object ignored
*/

t_state		destroy(int x, int y)
{
  t_game	*game;
  int		ret;

  game = get_game(NULL, FALSE);
  if (game->map[y][x]->type == SQUARE_WALL)
    ret = 0;
  ret = NEUTRAL;
  ret = destroy_player(game, x, y);
  if (ret != FAILURE)
    ret = g_destroy_elem[game->map[y][x]->type](game, x, y);
  if (store_explosion_coords(x, y) == FALSE)
    return (FAILURE);
  return (ret);
}
