#include <stdlib.h>
#include <SDL2/SDL.h>
#include <stdio.h>
#include <unistd.h>
#include "configuration.h"
#include "errors.h"
#include "game_loop.h"
#include "free_draw_game_stuff.h"
#include "server_loop.h"
#include "timer.h"
#include "game_draw.h"
#include "game.h"
#include "map.h"
#include "init_game_graphics.h"
#include "serialize.h"
#include "parse_server_frame.h"
#include "socket_compatibility.h"
#include "usleep_compatibility.h"

static t_key_scan	g_key_codes[] = {
  { SDL_SCANCODE_UP, SDLK_UP },
  { SDL_SCANCODE_RIGHT, SDLK_RIGHT },
  { SDL_SCANCODE_DOWN, SDLK_DOWN },
  { SDL_SCANCODE_LEFT, SDLK_LEFT },
  { 0, 0 }
};

static t_bool	handle_keystates(t_draw_game_stuff *dgs, int sock_fd)
{
  const Uint8	*keystate;
  int		i;

  i = 0;
  keystate = SDL_GetKeyboardState(NULL);
  while (g_key_codes[i].scan_code != 0)
    {
      if (keystate[g_key_codes[i].scan_code])
	{
	  if (SOCK_SEND(sock_fd, (char*)&g_key_codes[i].key_code, sizeof(int)) < 0)
	    return (my_bool_error(SCKET_ERROR));
	  dgs->players[dgs->player_id]->is_walking = TRUE;
	  return (TRUE);
	}
      ++i;
    }
  return (TRUE);
}

static t_bool	game_event_handler(t_draw_game_stuff *dgs, int sock_fd)
{
  SDL_Event	e;

  if (SDL_PollEvent(&e))
    {
      if (dgs->players[dgs->player_id]
	  && e.type == SDL_KEYDOWN
	  && e.key.keysym.sym == SDLK_SPACE)
	{
	  if (SOCK_SEND(sock_fd, (char*)&e.key.keysym.sym, sizeof(int)) < 0)
	    return (my_bool_error(SCKET_ERROR));
	}
      else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_ESCAPE)
	return (FALSE);
      else if (dgs->players[dgs->player_id])
	dgs->players[dgs->player_id]->is_walking = FALSE;
    }
  if (dgs->players[dgs->player_id]
      && handle_keystates(dgs, sock_fd) == FALSE)
    return (FALSE);
  return (TRUE);
}

t_game		*refresh_game(int sock_fd, t_draw_game_stuff *dgs)
{
  int		size;
  int		ret;
  char		*buffer;
  int		nb_read;
  t_game	*game;

  game = NULL;
  nb_read = 0;
  size = 1000 * sizeof(int);
  if ((buffer = malloc(size + 1)) == NULL)
    return (my_null_error(MEMORY_ERROR));
  while ((ret = SOCK_RECV(sock_fd, buffer, size)) > 0
	 && (nb_read = (ret + nb_read)) != size);
  if (nb_read == size)
    {
      if ((game = deserialize_game(buffer)) == NULL)
	return (NULL);
      if (dgs)
	update_dgs(game, dgs);
    }
  return (game);
}

char    *read_client_socket(int sock_fd)
{
  char	*buffer;
  int	size;
  int	nb_read;
  int	ret;

  size = MAX_SOCKET_LEN * sizeof(int);
  if ((buffer = malloc(size + 1)) == NULL)
    return (my_null_error(MEMORY_ERROR));
  nb_read = 0;
  while ((ret = SOCK_RECV(sock_fd, buffer, size)) > 0
	 && (nb_read = (ret + nb_read)) != size);
  if (nb_read > 0)
    return (buffer);
  return (NULL);
}

static t_bool   draw_and_event(int sock_fd, int elapsed, t_draw_game_stuff *dgs)
{
  if (game_event_handler(dgs, sock_fd) == FALSE
      || draw_game(dgs, elapsed, dgs->square_width, dgs->square_height) == FALSE)
    return (FALSE);
  return (TRUE);
}

t_bool			game_loop(SDL_Renderer *renderer, int sock_client_fd)
{
  t_bool		run;
  int			elapsed;
  int			usleep_value;
  t_game		*game;
  t_draw_game_stuff	*dgs;
  char			*frame_from_server;

  run = TRUE;
  init_timer(CLT_TIMER_ID);
  while ((game = refresh_game(sock_client_fd, NULL)) == NULL)
    usleep(500000);
  if ((dgs = init_game_graphics(renderer, game)) == NULL)
    return (FALSE);
  elapsed = 0;
  while (run && !game_is_over(game))
    {
      if ((usleep_value = ((1000 / FPS) * 1000) - (elapsed * 1000)) > 0)
	usleep(usleep_value);
      elapsed += usleep_value / 1000;
      if ((frame_from_server = read_client_socket(sock_client_fd)) != NULL)
	if (parse_server_frame(game, dgs, frame_from_server) == FALSE)
	  return (FALSE);
      if (draw_and_event(sock_client_fd, elapsed, dgs) == FALSE)
	run = FALSE;
      elapsed = update_timer(CLT_TIMER_ID);
    }
  free_draw_game_stuff(dgs);
  return (TRUE);
}
