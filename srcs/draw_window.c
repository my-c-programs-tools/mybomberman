#include "draw_window.h"
#include "menu_draw.h"

t_bool	draw_window(SDL_Renderer *renderer,
		    t_menu_content *menu_content, unsigned int elapsed)
{
  if (menu_content->is_in_game == FALSE &&
      draw_menu(renderer, menu_content, elapsed) == FALSE)
    return (FALSE);
  return (TRUE);
}
