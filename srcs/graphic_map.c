#include <stdlib.h>
#include "errors.h"
#include "graphic_map.h"
#include "texture_handler.h"
#include "sprite.h"
#include "configuration.h"

SDL_Rect	*get_square_sprite_pos(int pos_x, int pos_y,
				       int sprite_width_size, int sprite_height_size)
{
  SDL_Rect	*sprite_position;

  if ((sprite_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  sprite_position->x = pos_x * sprite_width_size;
  sprite_position->y = pos_y * sprite_height_size;
  sprite_position->w = sprite_width_size;
  sprite_position->h = sprite_height_size;
  return (sprite_position);
}

t_bool		init_square_sprite(t_square *square, t_coordinates *square_pos,
				   int sprite_width_size, int sprite_height_size)
{
  t_sprite	*square_sprite;
  SDL_Rect	*sprite_position;

  if ((square_sprite = malloc(sizeof(t_sprite))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  square->sprite = square_sprite;
  load_corresponding_texture(square);
  sprite_position = get_square_sprite_pos(square_pos->x, square_pos->y,
					  sprite_width_size, sprite_height_size);
  square->sprite->sprite_rect = *sprite_position;
  return (TRUE);
}

t_bool		init_map_sprites(SDL_Renderer *renderer, t_game *game)
{
  int		y;
  int		x;
  int		square_width;
  int		square_height;
  t_coordinates	*square_pos;

  y = 0;
  square_width = WINDOW_WIDTH / game->map_width;
  square_height = WINDOW_HEIGHT / game->map_height;
  if (init_textures_tabs(renderer) == FALSE)
    return (FALSE);
  if ((square_pos = malloc(sizeof(t_coordinates))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  while (y < game->map_height)
    {
      x = 0;
      while (x < game->map_width)
	{
	  square_pos->x = x;
	  square_pos->y = y;
	  init_square_sprite(game->map[y][x], square_pos, square_width, square_height);
	  x++;
	}
      y++;
    }
  return (TRUE);
}
