#include "serialize_tools.h"

int	integer_array_len(int *src)
{
  int	i;

  i = 0;
  while (src[i])
    i++;
  return (i);
}
