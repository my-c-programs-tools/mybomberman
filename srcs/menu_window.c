#include <unistd.h>
#include "configuration.h"
#include "draw_window.h"
#include "errors.h"
#include "free_menu_tools.h"
#include "menu_event.h"
#include "menu_window.h"
#include "timer.h"
#include "srcs_path.h"
#include "usleep_compatibility.h"

void	init_animated_sprites_properties(t_menu_content *menu_content)
{
  menu_content->menu_flying_bomber->animation_time.refresh_time = 2000;
  menu_content->menu_flying_bomber->animation_time.last_update = 0;
  menu_content->menu_flying_bomber->speed = 2;
  menu_content->menu_flying_bomber->spe_act = 0;
  menu_content->menu_flying_bomber->direction = DIRECTION_RIGHT;
  menu_content->menu_pirate->animation_time.refresh_time = 50;
  menu_content->menu_pirate->animation_time.last_update = 0;
  menu_content->menu_pirate->speed = 1;
  menu_content->menu_pirate->spe_act = 0;
  menu_content->menu_pirate->direction = DIRECTION_RIGHT;
  menu_content->bomb->animation_time.refresh_time = 1;
  menu_content->bomb->animation_time.last_update = 0;
  menu_content->bomb->speed = 10;
}

t_bool	init_music(t_menu_content *menu_content)
{
  if ((Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)) == -1 ||
      !(menu_content->menu_music = Mix_LoadMUS(LINK_TO_THE_PAST_MUSIC_PATH)) ||
      (Mix_PlayMusic(menu_content->menu_music, 1) == -1))
    {
      free_menu_content(menu_content);
      return (my_bool_error(LOAD_MUSIC_ERROR));
    }
  return (TRUE);
}

t_bool			init_window()
{
  SDL_Window		*win;
  SDL_Renderer		*renderer;
  t_menu_content	*menu_content;

  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO) != 0 || TTF_Init() != 0)
    return (my_bool_error(SDL_INIT_ERROR));
  if ((win = SDL_CreateWindow("BomberMan", 100, 100, WINDOW_WIDTH, WINDOW_HEIGHT, 0)) == NULL)
    return (my_bool_error(SDL_WIN_ERROR));
  if ((renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED)) == NULL)
    return (my_bool_error(SDL_RENDER_CREATE_ERROR));
  if ((menu_content = create_menu(renderer)) == NULL)
    return (FALSE);
  menu_content->selected_label = menu_content->menu_list->first;
  if (change_selected_label(renderer, menu_content, 0) == FALSE)
    {
      free_menu_content(menu_content);
      return (FALSE);
    }
  if (init_music(menu_content) == FALSE)
    return (FALSE);
  init_animated_sprites_properties(menu_content);
  menu_event_loop(renderer, menu_content);
  return (TRUE);
}

t_bool		event_handler(SDL_Renderer *renderer,
			      t_menu_content *menu_content)
{
  SDL_Event	e;

  if (menu_content->is_ip_field_selected == FALSE &&
      menu_content->is_nb_player_selected == FALSE)
    {
      if (SDL_PollEvent(&e))
	if (action_event_handler(renderer, menu_content, e) == FALSE)
	  return (FALSE);
    }
  else
    {
      SDL_StartTextInput();
      if (SDL_PollEvent(&e))
	if (text_input_event_handler(renderer, menu_content, e) == FALSE)
	  return (FALSE);
    }
  return (TRUE);
}

void		menu_event_loop(SDL_Renderer *renderer,
				t_menu_content *menu_content)
{
  t_bool	run;
  int		elapsed;
  int		usleep_value;

  run = TRUE;
  init_timer(CLT_TIMER_ID);
  while (run)
    {
      elapsed = update_timer(CLT_TIMER_ID);
      if (event_handler(renderer, menu_content) == FALSE
	  || draw_window(renderer, menu_content, elapsed) == FALSE)
	run = FALSE;
      if ((usleep_value = ((1000 / FPS) * 1000) - (elapsed * 1000)) > 0)
	usleep(usleep_value);
    }
  free_SDL(menu_content);
  free_menu_content(menu_content);
}
