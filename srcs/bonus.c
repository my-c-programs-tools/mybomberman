#include <stdlib.h>
#include "configuration.h"
#include "bonus.h"
#include "errors.h"
#include "bonus_player.h"
#include "bonus_bomb.h"
#include "bonus_random.h"
#include "percentages.h"
#include "communication.h"

static t_bonus_stat	g_bonus_chances[] = {
  { BONUS_SPEED, SPEED_SPWN_CHNC, 0, bonus_speed },
  { BONUS_RANGE, RANGE_SPWN_CHNC, 0, bonus_range },
  { BONUS_BOMB, BOMB_SPWN_CHNC, 0, bonus_bomb },
  { BONUS_BCROSS, BCROSS_SPWN_CHNC, 0, bonus_bcross },
  { BONUS_BDIAGONAL, BDIAGONAL_SPWN_CHNC, 0, bonus_bdiagonal },
  { BONUS_BSTAR, BSTAR_SPWN_CHNC, 0, bonus_bstar },
  { BONUS_BNUKE, BNUKE_SPWN_CHNC, 0, bonus_bnuke },
  { BONUS_BRANDOM, BRANDOM_SPWN_CHNC, 0, bonus_brandom },
  { -1, -1, -1, NULL }
};

t_bonus		*spawn_bonus(int x, int y)
{
  t_bonus	*bonus;
  int		i;
  double	rand_value;

  if ((bonus = malloc(sizeof(t_bonus))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  i = 0;
  rand_value = ((double)rand() / (double)(RAND_MAX)) * 100.0;
  while (g_bonus_chances[i].prct != -1 && rand_value > g_bonus_chances[i].prct)
    ++i;
  bonus->type = g_bonus_chances[i].type;
  bonus->x = x;
  bonus->y = y;
  rand_value = rand() % 100;
  bonus->malus = (rand_value <= MALUS_CHNC) ? (TRUE) : (FALSE);
  return (bonus);
}

t_bool		apply_bonus(t_game *game, t_player *player, t_bonus *bonus)
{
  t_bool	ret;

  g_bonus_chances[bonus->type].bonus_function(player, bonus);
  game->map[bonus->y][bonus->x]->type = SQUARE_FREE;
  game->map[bonus->y][bonus->x]->object = NULL;
  ret = send_gone_obj_to_client(GONE_OBJECT, bonus->x, bonus->y);
  free(bonus);
  return (ret);
}

void	init_bonus_percentages()
{
  init_percentages(g_bonus_chances);
}
