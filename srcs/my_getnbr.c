#include "utils.h"

int		my_getnbr(char *str)
{
  int		i;
  int		nb;
  int		neg;

  i = 0;
  nb = 0;
  neg = 0;
  while (str[i] != '\0')
    {
      if ((str[i] < '0' || str[i] > '9') && str[i] != '+' && str[i] != '-')
	return (((neg % 2) == 0) ? (nb) : ((nb) * -1));
      if (str[i] == '-')
	neg += 1;
      else if (str[i] != '+')
	{
	  nb = nb * 10;
	  nb = nb + str[i] - 48;
	}
      i++;
    }
  if (neg % 2 == 1)
    return (-1 * nb);
  return (nb);
}
