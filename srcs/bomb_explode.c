#include <stdlib.h>
#include "bomb_explode.h"
#include "destroy.h"

static t_axis	g_cross_axes[] = {
  LFT_AXIS,
  RGT_AXIS,
  DWN_AXIS,
  UP_AXIS,
  NULL_AXIS
};

static t_axis	g_diagonal_axes[] = {
  UP_LFT_AXIS,
  UP_RGT_AXIS,
  DWN_LFT_AXIS,
  DWN_RGT_AXIS,
  NULL_AXIS
};

static t_axis	g_star_axes[] = {
  LFT_AXIS,
  RGT_AXIS,
  DWN_AXIS,
  UP_AXIS,
  UP_LFT_AXIS,
  UP_RGT_AXIS,
  DWN_LFT_AXIS,
  DWN_RGT_AXIS,
  NULL_AXIS
};

static t_axis	*g_explosions[] = {
  g_cross_axes,
  g_diagonal_axes,
  g_star_axes
};

static t_bool	explode_axis(int x, int y, t_axis *axis, int distance)
{
  t_state	ret;
  t_game	*game;

  if (distance < 1)
    return (TRUE);
  game = get_game(NULL, FALSE);
  UNUSED(game);
  if (x >= game->map_width || x <= 0
      || y >= game->map_height || y <= 0)
    return (TRUE);
  if ((ret = destroy(x, y)) != NEUTRAL)
    return ((ret == FAILURE) ? (FALSE) : (TRUE));
  x += axis->x;
  y += axis->y;
 --distance;
 return (explode_axis(x, y, axis, distance));
}

t_bool		explode(t_bomb *bomb)
{
  int		x;
  int		y;
  int		i;
  t_bool	ret;

  i = 0;
  if (bomb->type == BOMB_NUKE)
    return (destroy_players());
  if (destroy(bomb->x, bomb->y) == FAILURE)
    return (FALSE);
  while (g_explosions[bomb->type][i].x != 0
	 || g_explosions[bomb->type][i].y != 0)
    {
      x = bomb->x + g_explosions[bomb->type][i].x;
      y = bomb->y + g_explosions[bomb->type][i].y;
      ret = explode_axis(x, y, &g_explosions[bomb->type][i], bomb->range);
      if (!ret)
	return (FALSE);
      ++i;
    }
  bomb->player->used_bombs -= 1;
  free(bomb);
  return (TRUE);
}
