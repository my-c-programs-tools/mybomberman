#include "bonus.h"
#include "errors.h"
#include "serialize.h"
#include "serialize_tools.h"
#include "store_explosion_coords.h"


char	*serialize_bonus_event(char *buffer, t_bonus *bonus)
{
  buffer = serialize_char(buffer, bonus->type);
  buffer = serialize_char(buffer, bonus->malus);
  buffer = serialize_int(buffer, bonus->x);
  buffer = serialize_int(buffer, bonus->y);
  return (buffer);
}

char	*serialize_gone_obj_event(char *buffer, int object_type,
				    int pos_x, int pos_y)
{
  buffer = serialize_char(buffer, object_type);
  buffer = serialize_int(buffer, pos_x);
  buffer = serialize_int(buffer, pos_y);
  return (buffer);
}

char	*serialize_bomb_released_event(char *buffer, int object_type, int bomb_type,
				       int pos_x, int pos_y)
{
  buffer = serialize_char(buffer, object_type);
  buffer = serialize_char(buffer, bomb_type);
  buffer = serialize_int(buffer, pos_x);
  buffer = serialize_int(buffer, pos_y);
  return (buffer);
}

char	*serialize_win_or_death_event(char *buffer, int object_type, int player_id)
{
  buffer = serialize_char(buffer, object_type);
  buffer = serialize_char(buffer, player_id);
  return (buffer);
}

char	*serialize_moved_player_event(char *buffer, t_player *player,
				     int object_type)
{
  buffer = serialize_char(buffer, object_type);
  buffer = serialize_double(buffer, player->x);
  buffer = serialize_double(buffer, player->y);
  buffer = serialize_char(buffer, player->direction);
  buffer = serialize_char(buffer, player->color);
  buffer = serialize_char(buffer, player->is_walking);
  return (buffer);
}

char		*serialize_bomb_explosion_event(char *buffer, int object_type)
{
  t_coordinates	*positions;
  int		positions_len;
  int		i;

  i = 0;
  positions = get_explosion_coords(NULL, FALSE);
  positions_len = nb_explosion_coords();
  buffer = serialize_char(buffer, object_type);
  buffer = serialize_int(buffer, positions_len);
  while (i < positions_len)
    {
      buffer = serialize_int(buffer, positions[i].x);
      buffer = serialize_int(buffer, positions[i].y);
      ++i;
    }
  return (buffer);
}
