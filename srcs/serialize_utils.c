#include <string.h>
#include <stdio.h>
#include "serialize.h"
#include "utils.h"
#include "game.h"
#include "bomb.h"
#include "bonus.h"
#include "errors.h"

char	*serialize_char(char *buffer, char x)
{
  buffer = memcpy(buffer, &x, sizeof(char));
  return (buffer + sizeof(char));
}

char	*serialize_int(char *buffer, int x)
{
  buffer = memcpy(buffer, &x, sizeof(int));
  return (buffer + sizeof(int));
}

char	*serialize_double(char *buffer, double x)
{
  buffer = memcpy(buffer, &x, sizeof(double));
  return (buffer + sizeof(double));
}

char	*deserialize_char(char *buffer, char *x)
{
  *x = *((char*)buffer);
  return (buffer + sizeof(char));
}

char	*deserialize_int(char *buffer, int *x)
{
  *x = *((int*)buffer);
  return (buffer + sizeof(int));
}

char	*deserialize_double(char *buffer, double *x)
{
  *x = *((double*)buffer);
  return (buffer + sizeof(double));
}
