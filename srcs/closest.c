#include "utils.h"

int	closest(int value, int cmp1, int cmp2)
{
  int	diff1;
  int	diff2;

  diff1 = ABS(value - cmp1);
  diff2 = ABS(value - cmp2);
  return ((diff1 > diff2) ? (diff2) : (diff1));
}
