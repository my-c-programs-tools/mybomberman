#include <stdlib.h>
#include <unistd.h>
#include "communication.h"
#include "errors.h"
#include "serialize.h"
#include "send_to_client.h"

t_bool	send_bonus_to_client(t_bonus *bonus)
{
  int	i;
  char	*event;

  if ((event = malloc(sizeof(int) * MAX_SOCKET_LEN)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  i = 0;
  serialize_bonus_event(event, bonus);
  while (g_client_sockets[i] != -1)
    if (send_event_to_client(g_client_sockets[i++], event) == FALSE)
      {
	free(event);
	return (FALSE);
      }
  free(event);
  return (TRUE);
}

t_bool	send_gone_obj_to_client(int object_type, int pos_x, int pos_y)
{
  int	i;
  char	*event;

  if ((event = malloc(sizeof(int) * MAX_SOCKET_LEN)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  i = 0;
  serialize_gone_obj_event(event, object_type, pos_x, pos_y);
  while (g_client_sockets[i] != -1)
    if (send_event_to_client(g_client_sockets[i++], event) == FALSE)
      {
	free(event);
	return (FALSE);
      }
  free(event);
  return (TRUE);
}

t_bool	send_bomb_released_to_client(int object_type, int bomb_type, int pos_x,
				     int pos_y)
{
  int	i;
  char	*event;

  if ((event = malloc(sizeof(int) * MAX_SOCKET_LEN)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  i = 0;
  serialize_bomb_released_event(event, object_type, bomb_type, pos_x, pos_y);
  while (g_client_sockets[i] != -1)
    if (send_event_to_client(g_client_sockets[i++], event) == FALSE)
      {
	free(event);
	return (FALSE);
      }
  free(event);
  return (TRUE);
}

t_bool	send_win_or_death_to_client(int object_type, int player_id)
{
  int	i;
  char	*event;

  if ((event = malloc(sizeof(int) * MAX_SOCKET_LEN)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  i = 0;
  serialize_win_or_death_event(event, object_type, player_id);
  while (g_client_sockets[i] != -1)
    if (send_event_to_client(g_client_sockets[i++], event) == FALSE)
      {
	free(event);
	return (FALSE);
      }
  free(event);
  return (TRUE);
}

t_bool	send_moved_player_to_client(t_player *player, int object_type)
{
  int	i;
  char	*event;

  if ((event = malloc(sizeof(int) * MAX_SOCKET_LEN)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  i = 0;
  serialize_moved_player_event(event, player, object_type);
  while (g_client_sockets[i] != -1)
    if (send_event_to_client(g_client_sockets[i++], event) == FALSE)
      {
	free(event);
	return (FALSE);
      }
  free(event);
  return (TRUE);
}

t_bool	send_bomb_explosion_to_client(int object_type)
{
  int	i;
  char	*event;

  if ((event = malloc(sizeof(int) * MAX_SOCKET_LEN)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  i = 0;
  serialize_bomb_explosion_event(event, object_type);
  while (g_client_sockets[i] != -1)
    if (send_event_to_client(g_client_sockets[i++], event) == FALSE)
      {
	free(event);
	return (FALSE);
      }
  free(event);
  return (TRUE);
}
