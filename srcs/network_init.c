#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "utils.h"
#include "network_init.h"
#include "errors.h"
#include "server_loop.h"
#include "srcs_path.h"
#include "communication.h"
#include "pthread_compatibility.h"
#include "socket_compatibility.h"

int g_client_sockets[] = {-1, -1, -1, -1};

t_bool			accept_clients(t_server_info *server_info)
{
  int			i;
  unsigned long int	mode;
  socklen_t	        addrlen;
  struct sockaddr_in	client_addr;

  addrlen = sizeof(client_addr);
  i = 0;
  mode = SOCKET_MODE;
  while (i < server_info->nb_players)
    {
      g_client_sockets[i] = accept(server_info->sock_fd,
				   (struct sockaddr*)&client_addr, &addrlen);
      if (SOCKET_CONF(g_client_sockets[i], mode) == -1)
        return (my_bool_error(SCKT_CNF_ERROR));
      i++;
    }
  return (TRUE);
}

static void	*thread_server(void *arg)
{
  t_game	*game;

  if (accept_clients((t_server_info*)arg) == FALSE)
    return (NULL);
  if (!(game = init_game(MAP_PATH, ((t_server_info*)arg)->nb_players))
      || !(server_loop(((t_server_info*)arg)->nb_players, game)))
    pthread_exit((void *)FALSE);
  close(((t_server_info*)arg)->sock_fd);
  pthread_exit((void *)TRUE);
  return (NULL);
}

static int		init_server_socket(int port, int nb_players)
{
  int			sock_fd;
  int			option_enabled;
  struct sockaddr_in	sin;
  struct protoent	*pe;

  option_enabled = 1;
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons(port);
  pe = getprotobyname("tcp");
  if ((sock_fd = socket(PF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (my_int_error(SCKET_ERROR, -1));
  if (sock_fd == -1)
    return (-1);
  if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR,
                 SET_SOCK_OPT(&option_enabled), sizeof(int)) == -1 ||
      bind(sock_fd, (struct sockaddr*)&sin, sizeof(sin)) == -1 ||
      listen(sock_fd, nb_players) == -1)
    return (my_int_error(SCKET_ERROR, -1));
  return (sock_fd);
}

t_bool			run_server(int port, int nb_players)
{
  pthread_t		thread;
  t_server_info		*server_info;

  server_info = malloc(sizeof(t_server_info));
  if (server_info == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((server_info->sock_fd = init_server_socket(port, nb_players)) == -1)
    return (FALSE);
  server_info->nb_players = nb_players;
  if (pthread_create(&thread, NULL, thread_server, server_info) == -1)
    return (my_bool_error(THREAD_ERROR));
  return (TRUE);
}

int			init_client(char *ip, int port)
{
  int			sock_fd;
  unsigned long int	mode;
  struct sockaddr_in	sin;

  mode = SOCKET_MODE;
  sock_fd = socket(PF_INET, SOCK_STREAM, 0);
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  sin.sin_addr.s_addr = inet_addr(ip);
  if (connect(sock_fd, (struct sockaddr *)&sin, sizeof(struct sockaddr_in)) != 0)
    return (my_int_error(SCKET_ERROR, -1));
  if (SOCKET_CONF(sock_fd, mode) == -1)
    return (my_int_error(SCKT_CNF_ERROR, -1));
  return (sock_fd);
}
