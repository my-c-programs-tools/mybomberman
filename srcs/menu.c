#include <stdlib.h>
#include "free_menu_tools.h"
#include "menu.h"
#include "menu_init.h"
#include "menu_event.h"
#include "messages.h"
#include "errors.h"

t_bool	back_to_main_menu(SDL_Renderer *renderer, t_menu_content *menu_content)
{
  clear_menu_labels(menu_content->menu_list);
  if ((menu_content->menu_list = init_menu_list()) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if (add_label_to_menu(menu_content->menu_list,
			LABEL_PLAY, renderer, 0) != TRUE ||
      add_label_to_menu(menu_content->menu_list,
			LABEL_MUSIC, renderer, 1) != TRUE ||
      add_label_to_menu(menu_content->menu_list,
			LABEL_QUIT, renderer, 2) != TRUE)
    return (my_bool_error(MEMORY_ERROR));
  menu_content->selected_label = menu_content->menu_list->first;
  if (change_selected_label(renderer, menu_content, 0) == FALSE)
    return (FALSE);
  menu_content->is_in_main_menu = TRUE;
  menu_content->is_in_play_menu = FALSE;
  menu_content->is_ip_field_selected = FALSE;
  menu_content->is_nb_player_selected = FALSE;  
  return (TRUE);
}

t_bool		add_label_to_menu(t_menu_list *menu_list, char *label_text,
				  SDL_Renderer *renderer, int label_number)
{
  t_menu_label	*new_label;

  if ((new_label = malloc(sizeof(t_menu_label))) == NULL)
    return (FALSE);
  SDL_Color label_color_unselected = {86, 115, 154, 0}; 
  new_label = create_new_label(&label_color_unselected, renderer,
			       label_text, label_number);
  add_label_to_menu_list(menu_list, new_label);
  return (TRUE);
}

t_menu_content		*create_menu(SDL_Renderer *renderer)
{
  t_menu_list		*menu_list;

  if ((menu_list = init_menu_list()) == NULL)
    return (my_null_error(MEMORY_ERROR));
  if (add_label_to_menu(menu_list, LABEL_PLAY, renderer, 0) != TRUE ||
      add_label_to_menu(menu_list, LABEL_MUSIC, renderer, 1) != TRUE ||
      add_label_to_menu(menu_list, LABEL_QUIT, renderer, 2) != TRUE)
    {
      clear_menu_labels(menu_list);
      free(menu_list);
      return (my_null_error(MEMORY_ERROR));
    }
  return (init_menu_content(renderer, menu_list));
}
