#include <stdlib.h>
#include "errors.h"
#include "serialize.h"

char	*deserialize_bonus_event(char *buffer, t_bonus *bonus, char object_type)
{
  char	malus;
  int	x;
  int	y;

  bonus->type = object_type;
  buffer = deserialize_char(buffer, &malus);
  bonus->malus = malus;
  buffer = deserialize_int(buffer, &x);
  bonus->x = x;
  buffer = deserialize_int(buffer, &y);
  bonus->y = y;
  return (buffer);
}

char	*deserialize_gone_obj_event(char *buffer, t_gone_obj_event *event,
				      char object_type)
{
  int	pos_x;
  int	pos_y;

  event->object_type = object_type;
  buffer = deserialize_int(buffer, &pos_x);
  event->pos_x = pos_x;
  buffer = deserialize_int(buffer, &pos_y);
  event->pos_y = pos_y;
  return (buffer);
}

char	*deserialize_bomb_released_event(char *buffer, t_bomb_released_event *event,
				      char object_type)
{
  int	pos_x;
  int	pos_y;
  char	bomb_type;

  event->object_type = object_type;
  buffer = deserialize_char(buffer, &bomb_type);
  event->bomb_type = bomb_type;
  buffer = deserialize_int(buffer, &pos_x);
  event->pos_x = pos_x;
  buffer = deserialize_int(buffer, &pos_y);
  event->pos_y = pos_y;
  return (buffer);
}


char	*deserialize_win_or_death_event(char *buffer, t_win_or_death_event *event,
					char object_type)
{
  char	player_id;

  event->object_type = object_type;
  buffer = deserialize_char(buffer, &player_id);
  event->player_id = player_id;
  return (buffer);
}


char		*deserialize_moved_player_event(char *buffer, t_player *player)
{
  double	pos_x;
  double	pos_y;
  char		player_direction;
  char		player_color;
  char		is_walking;

  buffer = deserialize_double(buffer, &pos_x);
  player->x = pos_x;
  buffer = deserialize_double(buffer, &pos_y);
  player->y = pos_y;
  buffer = deserialize_char(buffer, &player_direction);
  player->direction = player_direction;
  buffer = deserialize_char(buffer, &player_color);
  player->color = player_color;
  buffer = deserialize_char(buffer, &is_walking);
  player->is_walking = is_walking;
  return (buffer);
}


char		*deserialize_bomb_explosion_event(char *buffer, t_bomb_explosion_event *event)
{
  int		positions_len;
  t_coordinates	*positions;
  int		i;

  i = 0;
  buffer = deserialize_int(buffer, &positions_len);
  event->positions_len = positions_len;
  if ((positions = malloc(sizeof(t_coordinates) * positions_len)) == NULL)
    return (my_null_error(MEMORY_ERROR));
  while (i < positions_len)
    {
      buffer = deserialize_int(buffer, &(positions[i].x));
      buffer = deserialize_int(buffer, &(positions[i].y));
      ++i;
    }
  event->positions = positions;
  return (buffer);
}
