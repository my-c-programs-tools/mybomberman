#include <stdlib.h>
#include "configuration.h"
#include "errors.h"
#include "menu_draw.h"
#include "menu_flying_bomber.h"
#include "menu_animation.h"

t_bool	draw_menu_animation(SDL_Renderer *renderer,
			    t_animated_sprite *menu_animated_sprite)
{
  if (menu_animated_sprite->is_visible == TRUE)
    {
      if (menu_animated_sprite->direction == DIRECTION_RIGHT)
	{
	  if (SDL_RenderCopy(renderer, menu_animated_sprite->sprite_texture,
	    &menu_animated_sprite->sprite_rect,
	    &menu_animated_sprite->sprite_renderer_position) != 0)
	    return (FALSE);
	}
	else
	  {
	  SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL;
	  if (SDL_RenderCopyEx(renderer, menu_animated_sprite->sprite_texture,
	    &menu_animated_sprite->sprite_rect,
	    &menu_animated_sprite->sprite_renderer_position,
	    0, NULL, flip) != 0)
	    return (FALSE);
	}
    }
  return (TRUE);
}

SDL_Rect	*get_position(t_menu_content *menu_content,
				   SDL_Texture *logo)
{
  SDL_Rect	*logo_position;
  int		width;
  int		height;

  if ((logo_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  if (SDL_QueryTexture(logo, NULL, NULL, &width, &height) != 0)
    return (my_null_error(TEXTURE_QUERY_ERROR));
  logo_position->x = menu_content->selected_label->label_position.x - (width * 2);
  logo_position->y = menu_content->selected_label->label_position.y;
  logo_position->w = width;
  logo_position->h = height;
  return (logo_position);
}

t_bool	draw_selected_label_logo(SDL_Renderer *renderer,
				 t_menu_content *menu_content, t_sprite *logo)
{
  if (SDL_RenderCopy(renderer, logo->sprite_texture, NULL,
		     get_position(menu_content, logo->sprite_texture)) != 0)
    return (FALSE);
  return (TRUE);
}

t_bool	following_draw_menu(SDL_Renderer *renderer,
			    t_menu_content *menu_content)
{
  if (menu_content->is_in_main_menu == FALSE)
    if (SDL_RenderCopy(renderer, menu_content->back_information->label_texture,
		       NULL,  &menu_content->back_information->label_position) != 0)
      return (FALSE);
  if (draw_selected_label_logo(renderer, menu_content, menu_content->selected_label_logo)
      == FALSE)
    return (FALSE);
  if ((menu_content->is_ip_field_selected == TRUE &&
      SDL_RenderCopy(renderer, menu_content->ip_field->label_texture,
		     NULL,  &menu_content->ip_field->label_position) != 0) ||
      (menu_content->is_nb_player_selected == TRUE &&
      SDL_RenderCopy(renderer, menu_content->nb_player_field->label_texture,
		     NULL,  &menu_content->nb_player_field->label_position) != 0))
    return (FALSE);
  SDL_RenderPresent(renderer);
  return TRUE;
}

t_bool		draw_menu(SDL_Renderer *renderer, t_menu_content *menu_content,
			  unsigned int elapsed)
{
  t_menu_label	*tmp;

  tmp = menu_content->menu_list->first;
  if (SDL_RenderClear(renderer) != 0)
    return (my_bool_error(SDL_RENDER_CLEAR_ERROR));
  if (SDL_RenderCopy(renderer, menu_content->menu_background->sprite_texture, NULL, NULL)
      != 0 || SDL_RenderCopy(renderer, menu_content->menu_logo->sprite_texture, NULL,
			     &menu_content->menu_logo_position) != 0)
    return (FALSE);
  while (tmp != NULL && tmp != menu_content->menu_list->last->next)
    {
      if (SDL_RenderCopy(renderer, tmp->label_texture, NULL, &tmp->label_position) != 0)
	return (FALSE);
      tmp = tmp->next;
    }
  change_flying_bomber_position(menu_content->menu_flying_bomber, elapsed);
  draw_menu_animation(renderer, menu_content->menu_flying_bomber);
  animate_pirate(menu_content->menu_pirate, elapsed);
  draw_menu_animation(renderer, menu_content->menu_pirate);
  if (should_drop_bomb(menu_content, elapsed) == FALSE)
    return (FALSE);
  if (draw_menu_animation(renderer, menu_content->bomb) == FALSE)
    return (FALSE);
  return (following_draw_menu(renderer, menu_content));
}
