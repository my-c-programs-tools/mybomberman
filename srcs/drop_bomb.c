#include <stdlib.h>
#include "communication.h"
#include "drop_bomb.h"
#include "errors.h"

static void	init_bomb(t_bomb *bomb, t_player *player)
{
  bomb->x = (int)player->x;
  bomb->y = (int)player->y;
  bomb->type = player->bomb_type;
  bomb->range = player->bomb_range;
  bomb->elapsed_time = 0;
  bomb->player = player;
  player->used_bombs += 1;
}

t_bool		drop_bomb(t_game *game, t_player *player, unsigned int elapsed)
{
  t_bomb	*bomb;
  int	        size;

  UNUSED(elapsed);
  if (player->used_bombs >= player->bomb_limit
      || game->map[(int)player->y][(int)player->x]->type == SQUARE_BOMB)
    return (TRUE);
  size = 0;
  while (game->bombs[size])
    ++size;
  if ((game->bombs = realloc(game->bombs, (size + 2) * sizeof(t_bomb*))) == NULL
      || (bomb = malloc(sizeof(t_bomb))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  game->bombs[size] = bomb;
  game->bombs[size + 1] = NULL;
  game->map[(int)player->y][(int)player->x]->type = SQUARE_BOMB;
  game->map[(int)player->y][(int)player->x]->object = (void*)bomb;
  init_bomb(bomb, player);
  if (send_bomb_released_to_client(RELEASED_BOMB, (int)bomb->type, (int)player->x,
				(int)player->y) == FALSE)
    return (FALSE);
  return (TRUE);
}
