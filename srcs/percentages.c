#include "percentages.h"

static void	summarize_percentages(t_bonus_stat *chances)
{
  int		i;
  int		current;

  current = 0;
  while (chances[current + 1].prct != -1)
    ++current;
  while (current > 0)
    {
      i = 0;
      while (i < current)
	{
	  chances[current].prct += chances[i].prct;
	  ++i;
	}
      --current;
    }
}

void	init_percentages(t_bonus_stat *chances)
{
  int	i;
  int	total;

  i = 0;
  total = 0;
  while (chances[i].value != -1)
    {
      total += chances[i].value;
      ++i;
    }
  i = 0;
  while (chances[i].value != -1)
    {
      chances[i].prct = chances[i].value;
      chances[i].prct /= total;
      chances[i].prct *= 100;
      ++i;
    }
  summarize_percentages(chances);
}
