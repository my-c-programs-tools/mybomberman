#include <stdlib.h>
#include "configuration.h"
#include "errors.h"
#include "menu_animation.h"
#include "menu_informations.h"
#include "menu_init.h"
#include "messages.h"
#include "utils.h"
#include "srcs_path.h"

t_menu_list	*init_menu_list()
{
  t_menu_list	*new_list;

  if ((new_list = malloc(sizeof(t_menu_list))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  new_list->first = NULL;
  new_list->last = NULL;
  new_list->element_nb = 0;
  return (new_list);
}

void	init_animations_rect(t_menu_content *menu_content)
{
  menu_content->menu_flying_bomber->sprite_rect.x = 86;
  menu_content->menu_flying_bomber->sprite_rect.y = 0;
  menu_content->menu_flying_bomber->sprite_rect.w = 42;
  menu_content->menu_flying_bomber->sprite_rect.h = 36;
  menu_content->menu_pirate->sprite_rect.x = 0;
  menu_content->menu_pirate->sprite_rect.y = 0;
  menu_content->menu_pirate->sprite_rect.w = 16;
  menu_content->menu_pirate->sprite_rect.h = 32;
  menu_content->bomb->sprite_rect.x = 0;
  menu_content->bomb->sprite_rect.y = 0;
  menu_content->bomb->sprite_rect.w = 16;
  menu_content->bomb->sprite_rect.h = 16;
}

t_bool	init_menu_animation(SDL_Renderer *renderer,
			    t_menu_content *menu_content)
{
  if ((menu_content->menu_flying_bomber = malloc(sizeof(t_animated_sprite)))
      == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((menu_content->menu_pirate = malloc(sizeof(t_animated_sprite))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((menu_content->bomb = malloc(sizeof(t_animated_sprite))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  init_animations_rect(menu_content);
  menu_content->menu_flying_bomber->sprite_texture =
    IMG_LoadTexture(renderer, FLYING_BOMBER_PATH);
  menu_content->menu_flying_bomber->spe_act = 0;
  init_animation_properties(menu_content->menu_flying_bomber, 0, WINDOW_HEIGHT / 8);
  menu_content->menu_flying_bomber->direction = DIRECTION_RIGHT;
  menu_content->menu_pirate->sprite_texture =
    IMG_LoadTexture(renderer, MENU_PIRATE_PATH);
  menu_content->menu_pirate->spe_act = 0;
  init_animation_properties(menu_content->menu_pirate, 0, (WINDOW_HEIGHT / 3) * 2);
  menu_content->bomb->sprite_texture = IMG_LoadTexture(renderer, BOMB_PATH);
  menu_content->menu_flying_bomber->is_visible = TRUE;
  menu_content->menu_pirate->is_visible = TRUE;
  menu_content->bomb->is_visible = FALSE;
  return (TRUE);
}

t_bool		init_menu_properties(SDL_Renderer *renderer,
			     t_menu_content *menu_content)
{
  SDL_Rect	*menu_logo_position;

  menu_content->menu_background->sprite_texture =
    IMG_LoadTexture(renderer, MENU_BACKGROUND_PATH);
  menu_content->menu_logo->sprite_texture =
    IMG_LoadTexture(renderer, MENU_LOGO_PATH);
  menu_content->selected_label_logo->sprite_texture =
    IMG_LoadTexture(renderer, SELECTED_LABEL_LOGO_PATH);
  if (init_menu_animation(renderer, menu_content) != TRUE)
    return (FALSE);
  if ((menu_logo_position =
       get_logo_position(menu_content->menu_logo->sprite_texture)) == NULL)
    return (FALSE);
  menu_content->is_already_bomb = FALSE;
  menu_content->is_in_game = FALSE;
  menu_content->is_in_main_menu = TRUE;
  menu_content->menu_logo_position = *menu_logo_position;
  menu_content->is_ip_field_selected = FALSE;
  menu_content->is_nb_player_selected = FALSE;
  menu_content->is_in_play_menu = FALSE;
  return (TRUE);
}

t_menu_content		*init_menu_content(SDL_Renderer *renderer,
					   t_menu_list *menu_list)
{
  t_menu_content	*menu_content;
  
  if ((menu_content = malloc(sizeof(t_menu_content))) == NULL ||
      (menu_content->menu_background = malloc(sizeof(t_sprite))) == NULL ||
      (menu_content->menu_logo = malloc(sizeof(t_sprite))) == NULL ||
      (menu_content->back_information = malloc(sizeof(t_menu_label))) == NULL ||
      (menu_content->selected_label_logo = malloc(sizeof(t_sprite))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  menu_content->menu_list = menu_list;
  if ((menu_content->back_information =
       init_back_information(renderer, MESSAGE_BACK)) == NULL)
    return (NULL);
  if (init_menu_properties(renderer, menu_content) != TRUE)
    return (my_null_error(MEMORY_ERROR));
  return (menu_content);
}
