#include <stdlib.h>
#include <sys/time.h>
#include "timer.h"
#include "utils.h"

static struct timeval	*get_time(int id, t_bool init)
{
  static struct timeval	time[NB_TIMERS];

  if (init)
    gettimeofday(&(time[id]), NULL);
  return (&(time[id]));
}

void	init_timer(int id)
{
  get_time(id, TRUE);
}

unsigned int		update_timer(int id)
{
  struct timeval	*last_time;
  struct timeval	current_time;
  unsigned int		elapsed_time;

  last_time = get_time(id, FALSE);
  gettimeofday(&current_time, NULL);
  elapsed_time = (current_time.tv_sec - last_time->tv_sec) * 1000;
  elapsed_time += (current_time.tv_usec - last_time->tv_usec) / 1000;
  last_time->tv_sec = current_time.tv_sec;
  last_time->tv_usec = current_time.tv_usec;
  return (elapsed_time);
}
