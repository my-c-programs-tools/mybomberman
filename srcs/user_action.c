#include <SDL2/SDL.h>
#include "user_action.h"
#include "user_movement.h"
#include "drop_bomb.h"
#include "communication.h"

static t_action	g_actions[] = {
  { move_left, SDLK_LEFT },
  { move_right, SDLK_RIGHT },
  { move_up, SDLK_UP },
  { move_down, SDLK_DOWN },
  { drop_bomb, SDLK_SPACE },
  { NULL, -1 }
};

t_bool	user_action(t_player *player, int *input, int elapsed,
		    t_animation_time *animation_time)
{
  int	i;

  UNUSED(animation_time);
  i = 0;
  while (g_actions[i].fct != NULL && g_actions[i].input != *input)
    ++i;
  if (i >= 0 && i < 4)
    animation_time[player->color].last_update = 0;
  if (g_actions[i].fct != NULL)
    i = g_actions[i].fct(get_game(NULL, FALSE), player, elapsed);
  *input = -1;
  return (i);
}
