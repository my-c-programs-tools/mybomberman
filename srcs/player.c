#include <stdlib.h>
#include "player.h"
#include "errors.h"
#include "utils.h"
#include "game.h"
#include "configuration.h"

static t_coordinates	g_player_start[] = {
  {  0, -1 },
  { -1,  0 },
  {  0,  0 },
  { -1, -1 }
};

t_player	*init_player(t_game *game, int id)
{
  t_player	*player;
  double	x;
  double	y;

  if ((player = malloc(sizeof(t_player))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  player->color = id;
  x = g_player_start[id].x + ((g_player_start[id].x < 0) ? (-0.5) : (1.5));
  y = g_player_start[id].y + ((g_player_start[id].y < 0) ? (-0.5) : (1.5));
  player->x = (x < 0) ? (game->map_width + x) : (x);
  player->y = (y < 0) ? (game->map_height + y) : (y);
  player->speed = DFLT_P_SPEED;
  player->direction = DIRECTION_DOWN;
  player->bomb_limit = DFLT_P_BMB_LMIT;
  player->bomb_type = DFLT_P_BMB_TYPE;
  player->bomb_range = DFLT_P_BMB_RNGE;
  player->used_bombs = 0;
  player->is_walking = FALSE;
  return (player);
}

t_player	*get_player(t_game *game, int x, int y)
{
  int		i;

  i = 0;
  while (i < 4)
    {
      if (game->players[i]
	  && (int)game->players[i]->x == x
	  && (int)game->players[i]->y == y)
	return (game->players[i]);
      ++i;
    }
  return (NULL);
}
