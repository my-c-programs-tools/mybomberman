#include <stdlib.h>
#include "update_map.h"
#include "bonus.h"
#include "bomb.h"
#include "errors.h"
#include "utils.h"
#include "serialize.h"

static t_bool	create_empty_model(t_square *square)
{
  square->type = SQUARE_FREE;
  square->object = NULL;
  return (TRUE);
}

static t_bool	create_bonus_model(int type, char *frame, t_square *square)
{
  t_bonus	    *object;

  if ((object = malloc(sizeof(t_bonus))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  square->type = SQUARE_BONUS;
  deserialize_bonus_event(frame, object, type);
  square->object = object;
  return (TRUE);
}

static t_bool		    create_bomb_model(int type, char *frame, t_square *square)
{
  t_bomb		        *object;
  t_bomb_released_event	tmp_bomb_released;

  if ((object = malloc(sizeof(t_bomb))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  square->type = SQUARE_BOMB;
  deserialize_bomb_released_event(frame, &tmp_bomb_released, type);
  object->type = (t_bomb_type)tmp_bomb_released.bomb_type;
  object->x = tmp_bomb_released.pos_x;
  object->y = tmp_bomb_released.pos_y;
  square->object = object;
  return (TRUE);
}

t_bool	create_square_model(char object_type, char *frame,
				     t_square *square)
{
  int	ret;

  if (object_type >= 0 && object_type <= 7)
    ret = create_bonus_model(object_type, frame, square);
  else if (object_type == 8)
    ret = create_empty_model(square);
  else
    ret = create_bomb_model(object_type, frame, square);
  return (ret);
}

void	add_object_to_map(t_square ***map, t_square *model,
			  t_coordinates *coords)
{
  map[coords->y][coords->x]->object = model->object;
  map[coords->y][coords->x]->type = model->type;
}
