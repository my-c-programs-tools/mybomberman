#include <stdlib.h>
#include "configuration.h"
#include "errors.h"
#include "explosion_draw.h"
#include "game_draw.h"
#include "texture_handler.h"
#include "srcs_path.h"

SDL_Rect	*get_explosion_position(t_coordinates *pos, int square_width_size,
					int square_height_size)
{
  SDL_Rect	*explosion_position;

  if ((explosion_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  explosion_position->x = pos->x * square_width_size;
  explosion_position->y = pos->y * square_height_size;
  explosion_position->w = square_width_size;
  explosion_position->h = square_height_size;
  return (explosion_position);
}

t_bool		draw_explosion(SDL_Renderer *renderer, t_coordinates *pos,
			       int square_width_size, int square_height_size)
{
  SDL_Rect	*rect_explosion_dest;

  rect_explosion_dest =
    get_explosion_position(pos, square_width_size, square_height_size);
  if (SDL_RenderCopy(renderer, get_explosion_texture(),
		     NULL, rect_explosion_dest) != 0)
    return (FALSE);
  return (TRUE);
}

t_bool			draw_game_explosions(t_draw_game_stuff *dgs,
					     int elapsed, int square_width_size,
					     int square_height_size)
{
  t_ui_explosion	*tmp;
  t_coordinates		*pos;

  tmp = dgs->explosion_list->first;
  if ((pos = malloc(sizeof(t_coordinates))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  while (tmp != NULL)
    {
      tmp->elapsed_time += elapsed;
      if (tmp->elapsed_time < 500)
	{
	  pos->x = tmp->pos_x;
	  pos->y = tmp->pos_y;
	  if (draw_explosion(dgs->renderer, pos,
			     square_width_size, square_height_size) == FALSE)
	    return (FALSE);
	  tmp = tmp->next;
	}
      else
	tmp = my_free_explosion_node(dgs->explosion_list, tmp);
    }
  return (TRUE);
}
