#include "free_menu_tools.h"
#include "editable_label.h"
#include "errors.h"
#include "menu.h"
#include "menu_event.h"
#include "menu_init.h"
#include "menu_play.h"

t_bool	init_menu_play_list(SDL_Renderer *renderer, t_menu_list *menu_list)
{
  if (add_label_to_menu(menu_list, "CREATE GAME : ", renderer, 0) != TRUE ||
      add_label_to_menu(menu_list, "JOIN GAME : ", renderer, 1) != TRUE)
    return (my_bool_error(MEMORY_ERROR));
  return (TRUE);
}

t_bool	init_menu_play(SDL_Renderer *renderer, t_menu_content *menu_content)
{
  clear_menu_labels(menu_content->menu_list);
  if ((menu_content->menu_list = init_menu_list()) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if (init_menu_play_list(renderer, menu_content->menu_list) == FALSE)
    return (my_bool_error(MEMORY_ERROR));
  menu_content->selected_label = menu_content->menu_list->first;
  if (change_selected_label(renderer, menu_content, 0) == FALSE)
    return (FALSE);
  menu_content->is_in_main_menu = FALSE;
  menu_content->is_in_play_menu = TRUE;
  init_editable_menu_fields(renderer, menu_content);
  return (TRUE);
}
