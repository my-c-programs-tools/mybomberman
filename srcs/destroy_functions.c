#include <stdlib.h>
#include "communication.h"
#include "configuration.h"
#include "square.h"
#include "destroy.h"
#include "bomb_timer.h"
#include "bonus.h"

t_state	ignore_explosion(t_game *game, int x, int y)
{
  if (game->map[y][x]->type == SQUARE_WALL)
    return (SUCCESS);
  game->map[y][x]->is_exploding = TRUE;
  return (NEUTRAL);
}

t_state		destroy_player(t_game *game, int x, int y)
{
  t_player	*player;
  int		i;

  player = get_player(game, x, y);
  if (player)
    {
      i = 0;
      while (game->players[i] != player)
	++i;
      game->players[i] = NULL;
      free(player);
      if (send_win_or_death_to_client(DEATH_PLAYER, i) == FALSE)
	return (FAILURE);
      return ((PLAYER_TCH_STOP) ? (SUCCESS) : (NEUTRAL));
    }
  return (NEUTRAL);
}

t_state	destroy_bonus(t_game *game, int x, int y)
{
  game->map[y][x]->is_exploding = TRUE;
  if (!BONUS_DESTROYABLE)
    return ((BONUS_TCH_STOP) ? (SUCCESS) : (NEUTRAL));
  free(game->map[y][x]->object);
  game->map[y][x]->type = SQUARE_FREE;
  if (send_gone_obj_to_client(GONE_OBJECT, x, y) == FALSE)
    return (FAILURE);
  return ((BONUS_TCH_STOP) ? (SUCCESS) : (NEUTRAL));
}

t_state	destroy_bomb(t_game *game, int x, int y)
{
  game->map[y][x]->is_exploding = TRUE;
  if (!BOMB_DESTROYABLE)
    return ((BOMB_TCH_STOP) ? (SUCCESS) : (NEUTRAL));
  if (send_gone_obj_to_client(GONE_OBJECT, x, y) == FALSE)
    return (FAILURE);
  if (remote_trigger(game->map[y][x]->object) == FALSE)
    return (FAILURE);
  return ((BONUS_TCH_STOP) ? (SUCCESS) : (NEUTRAL));
}

t_state		destroy_wall(t_game *game, int x, int y)
{
  int		rand_number;
  t_bonus	*bonus;

  game->map[y][x]->type = SQUARE_FREE;
  game->map[y][x]->is_exploding = TRUE;
  if (send_gone_obj_to_client(GONE_OBJECT, x, y) == FALSE)
    return (FAILURE);
  rand_number = rand() % 100 + 1;
  if (rand_number < BONUS_SPWN_CHNC)
    {
      if ((bonus = spawn_bonus(x, y)) == NULL)
	return (FAILURE);
      game->map[y][x]->type = SQUARE_BONUS;
      game->map[y][x]->object = (void*)bonus;
      if (send_bonus_to_client(bonus) == FALSE)
	return (FAILURE);
    }
  return ((WALL_TCH_STOP) ? (SUCCESS) : (NEUTRAL));
}
