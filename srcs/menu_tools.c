#include <stdlib.h>
#include "configuration.h"
#include "errors.h"
#include "menu_tools.h"
#include "srcs_path.h"

t_menu_label	*create_new_label(SDL_Color *label_color, SDL_Renderer *renderer,
				  char *label_text, int label_number)
{
  t_menu_label	*new_label;
  TTF_Font	*label_font;
  SDL_Surface	*label_surface;
  SDL_Rect	*label_position;
  
  if ((new_label = malloc(sizeof(t_menu_label))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  if ((label_font = TTF_OpenFont(MENU_FONT_PATH, 30)) == NULL)
    return (my_null_error(OPEN_ERROR));
  if ((label_surface =
       TTF_RenderText_Solid(label_font, label_text, *label_color)) == NULL)
    return (my_null_error(RENDER_TEXT_ERROR));
  new_label->label_font = label_font;
  new_label->label_color = *label_color;
  new_label->label_text = label_text;
  new_label->label_surface = label_surface;
  if ((new_label->label_texture =
       SDL_CreateTextureFromSurface(renderer, label_surface)) == NULL)
    return (my_null_error(TEXTURE_ERROR));
  if ((label_position = get_label_position(new_label->label_texture,
					   label_number)) == NULL)
    return (NULL);
  new_label->label_position = *label_position;
  return (new_label);
}

void		add_label_to_menu_list(t_menu_list *menu_list,
				   t_menu_label *menu_label)
{
  t_menu_label	*tmp;

   if (menu_list->first == NULL)
    {
      menu_list->first = menu_label;
      menu_list->first->next = NULL;
      menu_list->first->prev = NULL;
      menu_list->last = menu_list->first;
     }
  else
    {
      tmp = menu_list->last;
      menu_label->next = NULL;
      menu_label->prev = NULL;
      menu_list->last = menu_label;
      tmp->next = menu_list->last;
      menu_list->last->prev = tmp;
    }
   menu_list->element_nb += 1;
}

SDL_Rect	*get_logo_position(SDL_Texture *logo)
{
  SDL_Rect	*logo_position;
  int		width;
  int		height;

  if ((logo_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  SDL_QueryTexture(logo, NULL, NULL, &width, &height);
  logo_position->x = (WINDOW_WIDTH / 2) - (width / 2);
  logo_position->y = (WINDOW_HEIGHT / 15);
  logo_position->w = width;
  logo_position->h = height;
  return (logo_position);
}

SDL_Rect	*get_label_position(SDL_Texture *label, int label_number)
{
  SDL_Rect	*label_position;
  int		width;
  int		height;

  if ((label_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  SDL_QueryTexture(label, NULL, NULL, &width, &height);
  label_position->x = (WINDOW_WIDTH / 2) - (width / 2);
  label_position->y = (WINDOW_HEIGHT / 2) - (height / 2) +
    (height * label_number * 2);
  label_position->w = width;
  label_position->h = height;
  return (label_position);
}

t_bool	update_label(SDL_Renderer *renderer, t_menu_label *label)
{
  if ((label->label_surface =
      TTF_RenderText_Solid(label->label_font, label->label_text,
			   label->label_color)) == NULL)
    return (my_bool_error(RENDER_TEXT_ERROR));
  if ((label->label_texture =
       SDL_CreateTextureFromSurface(renderer, label->label_surface)) == NULL)
    return (my_bool_error(TEXTURE_ERROR));
  return (TRUE);
}
