#include <stdlib.h>
#include <unistd.h>
#include "server_loop.h"
#include "timer.h"
#include "errors.h"
#include "user_action.h"
#include "bomb_timer.h"
#include "configuration.h"
#include "serialize.h"
#include "communication.h"
#include "socket_compatibility.h"
#include "usleep_compatibility.h"

static int	get_user_input(int sock_client_fd)
{
  int		sym;

  if (SOCK_RECV(sock_client_fd, (char*)&sym, sizeof(int)) == 0)
    return (-1);
  sym = (sym == 0) ? (-1) : (sym);
  return (sym);
}

static void	clean_explosions(t_game *game)
{
  int		x;
  int		y;

  y = 0;
  while (game->map[y])
    {
      x = 0;
      while (game->map[y][x])
	{
	  game->map[y][x]->is_exploding = FALSE;
	  ++x;
	}
      ++y;
    }
}

static t_bool	check_walking_players(t_game *game,
				      t_animation_time *animation_time,
				      int elapsed, int nb_players)
{
  int		i;

  i = 0;
  while (i < nb_players)
    {
      animation_time[i].last_update += elapsed;
      if (game->players[i])
	{
	  if ((animation_time[i].last_update < animation_time[i].refresh_time) &&
	      game->players[i]->is_walking == FALSE)
	    {
	      game->players[i]->is_walking = TRUE;
	      if (send_moved_player_to_client(game->players[i], MOVED_PLAYER) == FALSE)
		return (FALSE);
	    }
	  else if ((animation_time[i].last_update > animation_time[i].refresh_time) &&
		   game->players[i]->is_walking == TRUE)
	    {
	      game->players[i]->is_walking = FALSE;
	      if (send_moved_player_to_client(game->players[i], MOVED_PLAYER) == FALSE)
		return (FALSE);
	    }
	}
      i++;
    }
  return (TRUE);
}

t_animation_time	*init_animation_time(int nb_players)
{
  t_animation_time	*animation_time;
  int			i;

  if ((animation_time = malloc(sizeof(t_animation_time) * (nb_players + 1))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  i = 0;
  while (i < nb_players)
    {
      animation_time[i].last_update = 50;
      animation_time[i].refresh_time = 50;
      i++;
    }
  return (animation_time);
}

t_bool			server_loop(int nb_players, t_game *game)
{
  int			i;
  int			ret;
  unsigned int		elapsed;
  int			*user_inputs;
  int			usleep_value;
  t_animation_time	*animation_time;

  game = get_game(game, TRUE);
  if ((user_inputs = malloc(sizeof(int) * nb_players)) == NULL)
    return (FALSE);
  init_timer(SRV_TIMER_ID);
  if ((animation_time = init_animation_time(nb_players)) == NULL)
    return (FALSE);
  elapsed = 0;
  ret = send_serialized_game(game);
  while (ret && !game_is_over(game))
    {
      if ((usleep_value = (1000000 / FPS) - (elapsed * 1000)) > 0)
      	usleep(usleep_value);
      elapsed += usleep_value / 1000;
      i = -1;
      ret = check_walking_players(game, animation_time, elapsed, nb_players);
      while (ret && i++ < nb_players)
	if (game->players[i] &&
	    (user_inputs[i] = get_user_input(g_client_sockets[i])) != -1)
	  ret = user_action(game->players[i], &user_inputs[i],
			    elapsed, animation_time);
      if (ret)
	{
	  refresh_bombs(elapsed);
	  clean_explosions(game);
	  elapsed = update_timer(SRV_TIMER_ID);
	}
    }
  free_game(game);
  get_game(NULL, TRUE);
  return (TRUE);
}
