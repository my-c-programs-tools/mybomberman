#include <stdlib.h>
#include <stdio.h>
#include "game.h"
#include "utils.h"
#include "errors.h"
#include "map.h"

static t_bool	init_square(t_square **new_line, char *line, int i)
{
  t_square	*square;
  t_square_type	type;

  if ((square = malloc(sizeof(t_square))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  type = (t_square_type)line[i] - '0';
  if (type != SQUARE_FREE
      && type != SQUARE_WALL
      && type != SQUARE_DESTRUCTIBLE
      && type != SQUARE_BONUS
      && type != SQUARE_BOMB)
    return (my_bool_error(MAP_ERROR));
  square->type = type;
  square->object = NULL;
  square->is_exploding = FALSE;
  new_line[i] = square;
  return (TRUE);
}

static t_bool	fill_line(t_square ****map, int ret, char *line, int *map_size)
{
  int		i;
  t_square	**new_line;

  if (ret == 1 && line[0] == '\n')
    return (TRUE);
  *map_size += 1;
  if ((*map = realloc(*map, (*map_size + 1) * sizeof(t_square*))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  ret = (line[ret - 1] =='\n') ? (ret - 1) : (ret);
  if ((new_line = malloc((ret + 1) * sizeof(t_square))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  i = 0;
  while (i < ret)
    if (init_square(new_line, line, i++) == FALSE)
      {
	free(new_line);
	return (FALSE);
      }
  new_line[i] = NULL;
  (*map)[*map_size - 1] = new_line;
  (*map)[*map_size] = NULL;
  return (TRUE);
}

static t_bool	read_map(FILE *file, t_square ****map)
{
  char		*buffer;
  int		map_size;
  int		ret;
  size_t	buff_size;

  buffer = NULL;
  buff_size = 4096;
  map_size = 0;
  while ((ret = getline(&buffer, &buff_size, file)) != -1)
    if (fill_line(map, ret, buffer, &map_size) == FALSE)
      {
	free_map(*map);
	return (FALSE);
      }
  if (buffer)
    free(buffer);
  return (TRUE);
}

t_square	***init_map(char *filepath)
{
  FILE		*file;
  t_square	***map;

  if ((file = fopen(filepath, "r")) == NULL)
    return (my_null_error(OPEN_ERROR));
  map = NULL;
  read_map(file, &map);
  if (fclose(file) == EOF)
    {
      if (map)
	free_map(map);
      return (my_null_error(CLOSE_ERROR));
    }
  return (map);
}
