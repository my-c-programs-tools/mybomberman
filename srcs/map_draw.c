#include "errors.h"
#include "graphic_explosion_tools.h"
#include "map_draw.h"
#include "texture_handler.h"

t_bool	draw_square(SDL_Renderer *renderer, t_square *square)
{
  if (SDL_RenderCopy(renderer, square->sprite->sprite_texture,
		     NULL, &square->sprite->sprite_rect) != 0)
    return (FALSE);
  return (TRUE);
}

t_bool		check_for_explode(SDL_Renderer *renderer, t_square *square)
{
  SDL_Texture	*bomb_texture;

  if (square->type == SQUARE_BOMB)
    {
      bomb_texture = get_bomb_texture(square);
      if (SDL_RenderCopy(renderer, bomb_texture, NULL, &square->sprite->sprite_rect) != 0)
	return (FALSE);
    }
  return (TRUE);
}

t_bool	draw_map(t_draw_game_stuff *dgs)
{
  int	y;
  int	x;

  y = 0;
  while (dgs->map[y] != NULL)
    {
      x = 0;
      while (dgs->map[y][x] != NULL)
	{
	  load_corresponding_texture(dgs->map[y][x]);
	  if (draw_square(dgs->renderer, dgs->map[y][x]) == FALSE)
	    return (FALSE);
	  if (check_for_explode(dgs->renderer, dgs->map[y][x]) == FALSE)
	    return (FALSE);
	  if (dgs->map[y][x]->is_exploding == TRUE)
	    {
	      if (init_new_ui_explosion_and_add_it_to_list(dgs->explosion_list, x, y) == FALSE)
		return (FALSE);
	      dgs->map[y][x]->is_exploding = FALSE;
	    }
	  x++;
	}
      y++;
    }
  return (TRUE);
}
