#include <stdlib.h>
#include "init_game_graphics.h"
#include "graphic_explosion_tools.h"
#include "graphic_map.h"
#include "texture_handler.h"
#include "errors.h"
#include "configuration.h"

static t_bool	init_graphic_player(t_player *player, SDL_Renderer *renderer)
{
  if ((player->sprite = malloc(sizeof(t_player_sprite))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((player->sprite->sprite_texture =
       SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
			 SDL_TEXTUREACCESS_TARGET, 1, 1)) == NULL)
    {
      free(player);
      return (my_bool_error(TEXTURE_ERROR));
    }
  load_player_color_texture(player);
  player->sprite->sprite_rect.x = 0;
  player->sprite->sprite_rect.y = 0;
  player->sprite->animation_time.last_update = 0;
  player->sprite->animation_time.refresh_time = 50;
  return (TRUE);
}

static t_draw_game_stuff	*init_draw_game_stuff(SDL_Renderer *renderer,
						      t_game *game)
{
  t_draw_game_stuff		*dgs;
  int				i;

  if ((dgs = malloc(sizeof(t_draw_game_stuff))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  dgs->square_width = WINDOW_WIDTH / game->map_width;
  dgs->square_height = WINDOW_HEIGHT / game->map_height;
  dgs->renderer = renderer;
  dgs->map = game->map;
  dgs->player_id = game->player_id;
  if ((dgs->explosion_list = init_ui_explosion_list()) == NULL)
    {
      free(dgs);
      return (NULL);
    }
  dgs->players = game->players;
  i = 0;
  while (i < 4)
    {
      if (dgs->players[i])
	init_graphic_player(dgs->players[i], renderer);
      ++i;
    }
  return (dgs);
}

t_draw_game_stuff	*init_game_graphics(SDL_Renderer *renderer, t_game *game)
{
  t_draw_game_stuff	*dgs;

  if ((init_map_sprites(renderer, game)) == FALSE
      || (dgs = init_draw_game_stuff(renderer, game)) == NULL)
    return (NULL);
  return (dgs);
}

t_bool	init_textures(SDL_Renderer *renderer, t_texture_pack *textures)
{
  int	i;

  i = 0;
  while (textures[i].path)
    {
      if ((textures[i].texture =
	   SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
			     SDL_TEXTUREACCESS_TARGET, 1, 1)) == NULL)
	return (my_bool_error(MEMORY_ERROR));
      if ((textures[i].texture =
	   IMG_LoadTexture(renderer, textures[i].path)) == NULL)
	return (my_bool_error(TEXTURE_ERROR));
      ++i;
    }
  return (TRUE);
}

void	update_dgs(t_game *game, t_draw_game_stuff *dgs)
{
  int	x;
  int	y;

  x = 0;
  while (x < 4)
    {
      if (game->players[x])
	game->players[x]->sprite = dgs->players[x]->sprite;
      ++x;
    }
  dgs->players = game->players;
  y = 0;
  while (game->map[y])
    {
      x = 0;
      while (game->map[y][x])
	{
	  game->map[y][x]->sprite = dgs->map[y][x]->sprite;
	  ++x;
	}
      ++y;
    }
  dgs->map = game->map;
  dgs->player_id = game->player_id;
}
