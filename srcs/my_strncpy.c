#include "utils.h"

char	*my_strncpy(char *dest, char *src, int n)
{
  char	*saver;

  saver  = dest;
  while (n--)
    *saver++ = *src++;
  *saver = '\0'; 
  return (dest);
}
