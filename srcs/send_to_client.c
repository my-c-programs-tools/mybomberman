#include <stdlib.h>
#include <unistd.h>
#include "errors.h"
#include "send_to_client.h"
#include "serialize.h"
#include "socket_compatibility.h"

t_bool	send_event_to_client(int sock_client_fd, char *event)
{
  int	ret;
  int	size;
  int	wrote;

  wrote = 0;
  size = MAX_SOCKET_LEN * sizeof(int);
  while ((ret = SOCK_SEND(sock_client_fd, event, size)) > 0
	 && (wrote = (wrote + ret)) != size);
  if (ret < 0)
    return (my_bool_error(SCKET_ERROR));
  return (TRUE);
}

t_bool		send_serialized_game_to_client(int id, int sock_client_fd, t_game *game)
{
  char		*buffer;
  int		size;
  int		ret;
  int		wrote;

  size = 1000 * sizeof(int);
  if ((buffer = malloc(size)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  game->player_id = id;
  serialize_game(buffer, game);
  wrote = 0;
  while ((ret = SOCK_SEND(sock_client_fd, buffer, size)) > 0
	 && (wrote = (ret + wrote)) != size);
  if (ret < 0)
    return (my_bool_error(SCKET_ERROR));
  return (TRUE);
}

t_bool	send_serialized_game(t_game *game)
{
  int	i;

  i = 0;
  while (g_client_sockets[i] != -1)
    {
      if (send_serialized_game_to_client(i, g_client_sockets[i], game) == FALSE)
	return (FALSE);
      i++;
    }
  return (TRUE);
}
