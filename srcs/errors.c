#include <stdlib.h>
#include <unistd.h>
#include "utils.h"
#include "errors.h"

void		write_error(char *message)
{
  write(2, message, my_strlen(message));
}

t_bool		my_bool_error(char *message)
{
  write_error(message);
  return (FALSE);
}

int		my_int_error(char *message, int return_value)
{
  write_error(message);
  return (return_value);
}

void		*my_null_error(char *message)
{
  write_error(message);
  return (NULL);
}
