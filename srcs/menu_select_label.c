#include "errors.h"
#include "game_loop.h"
#include "menu_select_label.h"
#include "menu_select_label_tools.h"
#include "menu_play.h"
#include "menu_music.h"
#include "messages.h"
#include "network_init.h"
#include "free_menu_tools.h"

void	handle_selected_field(t_menu_content *menu_content)
{
  if (menu_content->is_in_play_menu == TRUE)
    {
      if (my_strcmp(menu_content->selected_label->label_text,
		    LABEL_CREATE_GAME) == 0)
	{
	  menu_content->is_nb_player_selected = TRUE;
	  menu_content->is_ip_field_selected = FALSE;
	}
      else if (my_strcmp(menu_content->selected_label->label_text,
			 LABEL_JOIN_GAME) == 0)
	{
	  menu_content->is_ip_field_selected = TRUE;
	  menu_content->is_nb_player_selected = FALSE;
	}
    }
}

t_bool		change_selected_label(SDL_Renderer *renderer,
				      t_menu_content *menu_content,
				      SDL_Keycode key)
{
  SDL_Color	color_unselected = {86, 115, 154, 0};
  SDL_Color	color_selected = {121, 248, 248, 0};
  t_menu_label	*tmp;

  tmp = menu_content->selected_label;
  if ((key == SDLK_UP && tmp->prev == NULL) || (key == SDLK_DOWN && tmp->next == NULL))
    return (TRUE);
  tmp->label_color = color_unselected;
  update_label(renderer, tmp);
  if (key == SDLK_UP && tmp->prev != NULL && tmp->prev->label_text != NULL)
    tmp = tmp->prev;
  else if (key == SDLK_DOWN && tmp->next != NULL && tmp->next->label_text != NULL)
    tmp = tmp->next;
  tmp->label_color = color_selected;
  menu_content->selected_label = tmp;
  if (update_label(renderer, tmp) == FALSE)
    return (FALSE);
  handle_selected_field(menu_content);
  return (TRUE);
}

t_bool	select_create_or_join(SDL_Renderer *renderer,
			      t_menu_content *menu_content)
{
  char	*ip;
  int	nb_players;
  int	sock_fd;

  if (my_strcmp(menu_content->selected_label->label_text, LABEL_CREATE_GAME) == 0)
    {
      if ((nb_players = get_nb_player_selected(menu_content)) == 0)
	return (TRUE);
      if (run_server(BOMBER_PORT, nb_players) == FALSE)
	return (FALSE);
      ip = LOCALHOST_IP;
    }
  else if (my_strcmp(menu_content->selected_label->label_text, LABEL_JOIN_GAME) == 0)
    ip = get_ip_selected(menu_content);
  else
    return (TRUE);
  if ((sock_fd = init_client(ip, BOMBER_PORT)) == -1)
    {
      init_menu_play(renderer, menu_content);
      return (game_launching_error(IP_ERROR));
    }
  if (game_loop(renderer, sock_fd) == FALSE)
    return (FALSE);
  init_menu_play(renderer, menu_content);
  return (TRUE);
}

t_bool	select_label(SDL_Renderer *renderer, t_menu_content *menu_content)
{
  if (my_strcmp(menu_content->selected_label->label_text, LABEL_PLAY) == 0)
    {
      if (init_menu_play(renderer, menu_content) == FALSE)
	return (FALSE);
    }
  else if (my_strcmp(menu_content->selected_label->label_text, LABEL_MUSIC) == 0)
    {
      if (init_menu_music(renderer, menu_content) == FALSE)
	return (FALSE);
    }
  else if ((menu_content->is_in_main_menu == FALSE &&
	    (select_soundtrack(menu_content) == FALSE ||
	     select_create_or_join(renderer, menu_content) == FALSE)) ||
	   my_strcmp(menu_content->selected_label->label_text, LABEL_QUIT) == 0)
    return (FALSE);
  return (TRUE);
}
