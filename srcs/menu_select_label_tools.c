#include "errors.h"
#include "menu_select_label_tools.h"
#include "messages.h"

int	get_nb_player_selected(t_menu_content *menu_content)
{
  int	nb_players;

  nb_players = 0;
  if (my_strlen(menu_content->nb_player_field->label_text) > 1)
    my_remove_n_first_char(menu_content->nb_player_field->label_text, 1);
  nb_players = my_getnbr(menu_content->nb_player_field->label_text);
  if (nb_players < 2 || nb_players > 4)
    return (game_launching_error(NB_PLAYER_ERROR));
  return (nb_players);
}

char	*get_ip_selected(t_menu_content *menu_content)
{
  if (my_strlen(menu_content->ip_field->label_text) > 1)
    my_remove_n_first_char(menu_content->ip_field->label_text, 1);
  return (menu_content->ip_field->label_text);
}

t_bool	game_launching_error(char *message)
{
  if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, TITLE_ERROR, message, NULL) < 0)
    return (my_bool_error(MESSAGEBOX_ERROR));
  return (TRUE);
}
