#include	"bonus_player.h"

void	bonus_speed(t_player *player, t_bonus *bonus)
{
  int   value;

  value = (bonus->malus) ? (-1) : (1);
  player->speed += value;
  if (player->speed < 1)
    player->speed = 1;
}

void	bonus_range(t_player *player, t_bonus *bonus)
{
  int   value;

  value = (bonus->malus) ? (-1) : (1);
  player->bomb_range += value;
  if (player->bomb_range < 1)
    player->bomb_range = 1;
}

void	bonus_bomb(t_player *player, t_bonus *bonus)
{
  int   value;

  value = (bonus->malus) ? (-1) : (1);
  player->bomb_limit += value;
  if (player->bomb_limit < 1)
    player->bomb_limit = 1;
}
