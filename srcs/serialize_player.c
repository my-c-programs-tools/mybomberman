#include "serialize.h"

char	*serialize_player(char *buffer, t_player *player)
{
  buffer = serialize_double(buffer, player->x);
  buffer = serialize_double(buffer, player->y);
  buffer = serialize_int(buffer, player->direction);
  buffer = serialize_int(buffer, player->color);
  return (buffer);
}

char 	    *deserialize_player(char *buffer, t_player *player)
{
  double	x;
  double	y;
  int		direction;
  int	    color;

  buffer = deserialize_double(buffer, &x);
  player->x = x;
  buffer = deserialize_double(buffer, &y);
  player->y = y;
  buffer = deserialize_int(buffer, &direction);
  player->direction = (t_direction)direction;
  buffer = deserialize_int(buffer, &color);
  player->color = color;
  return (buffer);
}
