#include <stdlib.h>
#include "free_menu_tools.h"

void	free_SDL(t_menu_content *menu_content)
{
  SDL_Quit();
  TTF_Quit();
  Mix_FreeMusic(menu_content->menu_music);
  Mix_Quit();
}

void	free_label(t_menu_label *label)
{
  free(label->label_font);
  free(label->label_surface);
  free(label->label_texture);
  free(label);
}

void		clear_menu_labels(t_menu_list *menu_list)
{
  t_menu_label	*tmp;

  tmp = menu_list->first;
  while (tmp != NULL && tmp != menu_list->last->next)
    {
      tmp = tmp->next;
      if (tmp != NULL)
	{
	  free_label(tmp->prev);
	  menu_list->element_nb -= 1;
	}
    }
}

void	free_menu_content(t_menu_content *menu_content)
{
  clear_menu_labels(menu_content->menu_list);
  free(menu_content->menu_list);
  if (menu_content->back_information)
    free_label(menu_content->back_information);
  SDL_DestroyTexture(menu_content->menu_background->sprite_texture);
  free(menu_content->menu_background);
  SDL_DestroyTexture(menu_content->menu_logo->sprite_texture);
  free(menu_content->menu_logo);
  SDL_DestroyTexture(menu_content->selected_label_logo->sprite_texture);
  free(menu_content->selected_label_logo);
  SDL_DestroyTexture(menu_content->menu_flying_bomber->sprite_texture);
  free(menu_content->menu_flying_bomber);
  SDL_DestroyTexture(menu_content->menu_pirate->sprite_texture);
  free(menu_content->menu_pirate);
  SDL_DestroyTexture(menu_content->bomb->sprite_texture);
  free(menu_content->bomb);
  free(menu_content);
}
