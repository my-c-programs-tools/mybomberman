#include <stdlib.h>
#include "configuration.h"
#include "bonus_bomb.h"
#include "bonus_random.h"
#include "percentages.h"

static t_bonus_stat	g_bomb_chances[] = {
  { BONUS_BNUKE, BNUKE_SPWN_CHNC, 0, bonus_bnuke },
  { BONUS_BCROSS, BCROSS_SPWN_CHNC, 0, bonus_bcross },
  { BONUS_BDIAGONAL, BDIAGONAL_SPWN_CHNC, 0, bonus_bdiagonal },
  { BONUS_BSTAR, BSTAR_SPWN_CHNC, 0, bonus_bstar },
  { -1, -1, -1, NULL }
};

void		bonus_brandom(t_player *player, t_bonus *bonus)
{
  int		i;
  double	rand_value;

  UNUSED(bonus);
  i = 0;
  rand_value = ((double)rand() / (double)(RAND_MAX)) * 100.0;
  while (g_bomb_chances[i].prct != -1 && rand_value > g_bomb_chances[i].prct)
    ++i;
  g_bomb_chances[i].bonus_function(player, bonus);
}

void	init_bomb_percentages()
{
  init_percentages(g_bomb_chances);
}
