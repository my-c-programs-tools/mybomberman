#include <stdlib.h>
#include "game.h"

void	free_game(t_game *game)
{
  free_players(game->players);
  free_map(game->map);
}

void	free_players(t_player **players)
{
  int	i;

  i = 0;
  while (i < 4)
    {
      free(players[i]);
      ++i;
    }
}

void	free_map(t_square ***map)
{
  int	x;
  int	y;

  if (map == NULL)
    return ;
  y = 0;
  while (map[y])
    {
      x = 0;
      while (map[y][x])
	{
	  if (map[y][x]->type == SQUARE_BONUS
	      || map[y][x]->type == SQUARE_BOMB)
	    free(map[y][x]->object);
	  free(map[y][x]);
	  ++x;
	}
      free(map[y]);
      ++y;
    }
  free(map);
}
