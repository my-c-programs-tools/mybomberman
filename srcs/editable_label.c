#include <stdlib.h>
#include "configuration.h"
#include "editable_label.h"
#include "messages.h"
#include "errors.h"
#include "srcs_path.h"

SDL_Rect	*get_editable_label_position(t_menu_label *editable_label)
{
  SDL_Rect	*label_position;
  int		width;
  int		height;

  if ((label_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  SDL_QueryTexture(editable_label->label_texture, NULL, NULL, &width, &height);
  label_position->x = editable_label->label_position.x;
  label_position->y = editable_label->label_position.y;
  label_position->w = width;
  label_position->h = height;
  return (label_position);
}

t_bool		update_editable_label(SDL_Renderer *renderer, t_menu_label *label)
{
  SDL_Rect	*label_position;
  
  if (label->label_text != NULL &&
      (label->label_surface = TTF_RenderText_Solid(label->label_font, label->label_text,
						   label->label_color)) == NULL)
    return (my_bool_error(RENDER_TEXT_ERROR));
  if ((label->label_texture =
       SDL_CreateTextureFromSurface(renderer, label->label_surface)) == NULL)
    return (my_bool_error(TEXTURE_ERROR));
  if ((label_position = get_editable_label_position(label)) == NULL)
    return (FALSE);
  label->label_position = *label_position;
  return (TRUE);
}

void		set_editable_fields_position(t_menu_content *menu_content)
{
  int		width;
  int		height;
  t_menu_label	*tmp;

  tmp = menu_content->menu_list->first;
  while (tmp != NULL && tmp != menu_content->menu_list->last->next)
    {
      if (my_strcmp(tmp->label_text, LABEL_CREATE_GAME) == 0)
	{
	  SDL_QueryTexture(menu_content->nb_player_field->label_texture,
			   NULL, NULL, &width, &height);
	  menu_content->nb_player_field->label_position.x =
	    tmp->label_position.x + tmp->label_position.w;
	  menu_content->nb_player_field->label_position.y = tmp->label_position.y;
	  menu_content->nb_player_field->label_position.w = width;
	  menu_content->nb_player_field->label_position.h = height;
	}
      else if (my_strcmp(tmp->label_text, LABEL_JOIN_GAME) == 0)
	{
	  SDL_QueryTexture(menu_content->ip_field->label_texture,
			   NULL, NULL, &width, &height);
	  menu_content->ip_field->label_position.x =
	    tmp->label_position.x + tmp->label_position.w;
	  menu_content->ip_field->label_position.y = tmp->label_position.y;
	  menu_content->ip_field->label_position.w = width;
	  menu_content->ip_field->label_position.h = height;
	}
      tmp = tmp->next;
    }
}

t_bool		set_editable_properties(SDL_Renderer *renderer, t_menu_content *menu_content,
					TTF_Font *label_font, SDL_Color *label_color)
{
  SDL_Surface	*label_surface;
  
  if ((label_surface = TTF_RenderText_Solid(label_font, DEFAULT_IP, *label_color)) == NULL)
    return (my_bool_error(RENDER_TEXT_ERROR));
  menu_content->ip_field->label_font = label_font;
  menu_content->ip_field->label_color = *label_color;
  my_strcpy(menu_content->ip_field->label_text, DEFAULT_IP);
  menu_content->ip_field->label_surface = label_surface;
  if ((menu_content->ip_field->label_texture =
       SDL_CreateTextureFromSurface(renderer, label_surface)) == NULL)
    return (my_bool_error(TEXTURE_ERROR));
  if ((label_surface = TTF_RenderText_Solid(label_font, DEFAULT_NB_PLAYER,
					    *label_color)) == NULL)
    return (my_bool_error(RENDER_TEXT_ERROR));
  menu_content->nb_player_field->label_font = label_font;
  menu_content->nb_player_field->label_color = *label_color;
  my_strcpy(menu_content->nb_player_field->label_text, DEFAULT_NB_PLAYER);
  menu_content->nb_player_field->label_surface = label_surface;
  if ((menu_content->nb_player_field->label_texture =
       SDL_CreateTextureFromSurface(renderer, label_surface)) == NULL)
    return (my_bool_error(TEXTURE_ERROR));
  set_editable_fields_position(menu_content);
  return (TRUE);
}

t_bool		init_editable_menu_fields(SDL_Renderer *renderer,
				     t_menu_content* menu_content)
{
  TTF_Font	*label_font;

  SDL_Color label_color = {116, 208, 241, 0};
  if ((menu_content->ip_field = malloc(sizeof(t_menu_label))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((menu_content->nb_player_field = malloc(sizeof(t_menu_label))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((menu_content->ip_field->label_text = malloc(sizeof(char) * 4))
      == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((menu_content->nb_player_field->label_text = malloc(sizeof(char) * 11))
      == NULL)
    return (my_bool_error(MEMORY_ERROR));
  if ((label_font = TTF_OpenFont(MENU_FONT_PATH, 30)) == NULL)
    return (my_bool_error(OPEN_ERROR));
  if (set_editable_properties(renderer, menu_content, label_font, &label_color) == FALSE)
    return (FALSE);
  menu_content->is_nb_player_selected = TRUE;
  return (TRUE);
}
