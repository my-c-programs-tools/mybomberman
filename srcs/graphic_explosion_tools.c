#include <stdlib.h>
#include "errors.h"
#include "graphic_explosion_tools.h"

t_ui_explosion_list	*init_ui_explosion_list()
{
  t_ui_explosion_list	*new_list;

  if ((new_list = malloc(sizeof(t_ui_explosion_list))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  new_list->first = NULL;
  new_list->last = NULL;
  new_list->element_nb = 0;
  return (new_list);
}

void			add_ui_explosion_to_list(t_ui_explosion_list *explosion_list,
						 t_ui_explosion *explosion)
{
  t_ui_explosion	*tmp;

   if (explosion_list->first == NULL)
    {
      explosion_list->first = explosion;
      explosion_list->first->next = NULL;
      explosion_list->first->prev = NULL;
      explosion_list->last = explosion_list->first;
     }
  else
    {
      tmp = explosion_list->last;
      explosion->next = NULL;
      explosion->prev = NULL;
      explosion_list->last = explosion;
      tmp->next = explosion_list->last;
      explosion_list->last->prev = tmp;
    }
   explosion_list->element_nb += 1;
}

t_bool			init_new_ui_explosion_and_add_it_to_list(t_ui_explosion_list *explosion_list,
								 int pos_x, int pos_y)
{
  t_ui_explosion	*explosion;

  if ((explosion = malloc(sizeof(t_ui_explosion))) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  explosion->pos_x = pos_x;
  explosion->pos_y = pos_y;
  explosion->elapsed_time = 0;
  add_ui_explosion_to_list(explosion_list, explosion);
  return (TRUE);
}

t_ui_explosion		*my_free_explosion_node(t_ui_explosion_list *explosion_list,
					       t_ui_explosion *node)
{
  t_ui_explosion	*tmp;

  if (node == explosion_list->first && node->next != NULL)
    {
      explosion_list->first = node->next;
      explosion_list->first->prev = NULL;
      tmp = explosion_list->first;
    }
  else if (explosion_list->element_nb == 1)
    {
      explosion_list->first = NULL;
      explosion_list->last = NULL;
      tmp = NULL;
    }
  else if (explosion_list->element_nb >= 2)
    {
      node->prev->next = node->next;
      tmp = node->next;
    }
  free(node);
  explosion_list->element_nb -= 1;
  return (tmp);
}

void			free_ui_explosion_list(t_ui_explosion_list *explosion_list)
{
  t_ui_explosion	*tmp;
  
  while (explosion_list->first != NULL && explosion_list->first->next != NULL)
    {
      tmp = explosion_list->first;
      explosion_list->first = explosion_list->first->next;
      free(tmp);
    }
  free(explosion_list->first);
}
