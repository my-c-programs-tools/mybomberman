#include <stdlib.h>
#include "store_explosion_coords.h"
#include "errors.h"

int		        nb_explosion_coords()
{
  int		    i;
  t_coordinates	*coords;

  if ((coords = get_explosion_coords(NULL, FALSE)) == NULL)
    return (0);
  i = 0;
  while (coords[i].x != -1 && coords[i].y != -1)
    ++i;
  return (i);
}

t_coordinates		    *get_explosion_coords(t_coordinates *object,
					      t_bool init)
{
  static t_coordinates	*coords = NULL;

  if (init)
    coords = object;
  return (coords);
}

t_bool		store_explosion_coords(int x, int y)
{
  int		i;
  t_coordinates	*coords;

  if ((coords = get_explosion_coords(NULL, FALSE)) == NULL)
    {
      if ((coords = malloc(2 * sizeof(t_coordinates))) == NULL)
	return (my_bool_error(MEMORY_ERROR));
      get_explosion_coords(coords, TRUE);
      i = 1;
    }
  else
    {
      i = nb_explosion_coords() + 1;
      if ((coords = realloc(coords, (i + 1) * sizeof(t_coordinates))) == NULL)
	{
	  free_explosion_coords();
	  return (my_bool_error(MEMORY_ERROR));
	}
      get_explosion_coords(coords, TRUE);
    }
  coords[i - 1].x = x;
  coords[i - 1].y = y;
  coords[i].x = -1;
  coords[i].y = -1;
  return (TRUE);
}

void		    free_explosion_coords()
{
  t_coordinates	*coords;

  if ((coords = get_explosion_coords(NULL, FALSE)) == NULL)
    return ;
  free(coords);
}
