#include "bonus.h"
#include "errors.h"
#include "texture_handler.h"
#include "srcs_path.h"
#include "init_game_graphics.h"

static t_texture_pack	g_background_textures[] = {
  {NULL, FREE_TEXTURE_PATH},
  {NULL, WALL_TEXTURE_PATH},
  {NULL, DESTRUCTIBLE_TEXTURE_PATH},
  {NULL, NULL}
};

static t_texture_pack	g_bonus_textures[] = {
  {NULL, B_SPEED_TEXTURE_PATH},
  {NULL, B_RANGE_TEXTURE_PATH},
  {NULL, B_BOMB_TEXTURE_PATH},
  {NULL, B_BOMB_CROSS_TEXTURE_PATH},
  {NULL, B_BOMB_DIAGONAL_TEXTURE_PATH},
  {NULL, B_BOMB_STAR_TEXTURE_PATH},
  {NULL, B_BOMB_NUKE_TEXTURE_PATH},
  {NULL, B_BOMB_RANDOM_TEXTURE_PATH},
  {NULL, NULL}
};

static t_texture_pack	g_players_textures[] = {
  {NULL, P_DARK_TEXTURE_PATH},
  {NULL, P_GRAY_BLUE_TEXTURE_PATH},
  {NULL, P_ORANGE_TEXTURE_PATH},
  {NULL, P_PINK_TEXTURE_PATH},
  {NULL, NULL}
};

static t_texture_pack	g_bomb_textures[] = {
  {NULL, B_CROSS_TEXTURE_PATH},
  {NULL, B_DIAGONAL_TEXTURE_PATH},
  {NULL, B_STAR_TEXTURE_PATH},
  {NULL, B_NUKE_TEXTURE_PATH},
  {NULL, NULL}
};

static t_texture_pack	g_explosion_textures[] = {
  { NULL, EXPLOSION_TEXTURE_PATH },
  { NULL, NULL }
};

t_bool	init_textures_tabs(SDL_Renderer *renderer)
{
  if (init_textures(renderer, g_background_textures) == FALSE
      || init_textures(renderer, g_bonus_textures) == FALSE
      || init_textures(renderer, g_bomb_textures) == FALSE
      || init_textures(renderer, g_players_textures) == FALSE
      || init_textures(renderer, g_explosion_textures) == FALSE)
    return (FALSE);
  return (TRUE);
}

void	load_corresponding_texture(t_square *square)
{
  if (square->type >= 0 && square->type <= 2)
    square->sprite->sprite_texture = g_background_textures[square->type].texture;
  else if (square->type == 4)
    square->sprite->sprite_texture = g_background_textures[0].texture;
  else if (square->type == 3)
    {
      square->sprite->sprite_texture =
	g_bonus_textures[((t_bonus*) square->object)->type].texture;
    }
}

void	load_player_color_texture(t_player *player)
{
  player->sprite->sprite_texture = g_players_textures[player->color].texture;
}

SDL_Texture	*get_bomb_texture(t_square *square)
{
  return (g_bomb_textures[((t_bomb*) square->object)->type].texture);
}

SDL_Texture	*get_explosion_texture()
{
  return (g_explosion_textures[0].texture);
}
