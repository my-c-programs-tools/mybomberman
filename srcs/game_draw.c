#include "errors.h"
#include "explosion_draw.h"
#include "game_draw.h"
#include "map_draw.h"
#include "sprite.h"
#include "players_draw.h"

t_bool	draw_game(t_draw_game_stuff *dgs, int elapsed,
		  int square_width_size, int square_height_size)
{
  if (SDL_RenderClear(dgs->renderer) != 0)
    return (my_bool_error(SDL_RENDER_CLEAR_ERROR));
  if (draw_map(dgs) == FALSE ||
      draw_game_explosions(dgs, elapsed, square_width_size, square_height_size)
      == FALSE ||
      draw_players(dgs, elapsed, square_width_size, square_height_size) == FALSE)
    return (FALSE);
  SDL_RenderPresent(dgs->renderer);
  return (TRUE);
}
