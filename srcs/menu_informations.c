#include <stdlib.h>
#include "configuration.h"
#include "errors.h"
#include "menu_informations.h"
#include "srcs_path.h"

SDL_Rect	*get_label_info_position(SDL_Texture *label)
{
  SDL_Rect	*label_position;
  int		width;
  int		height;

  if ((label_position = malloc(sizeof(SDL_Rect))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  SDL_QueryTexture(label, NULL, NULL, &width, &height);
  label_position->x = (WINDOW_WIDTH / 2) - (width / 2);
  label_position->y = WINDOW_HEIGHT - (height * 2);
  label_position->w = width;
  label_position->h = height;
  return (label_position);
}

t_menu_label	*init_back_information(SDL_Renderer *renderer,
				      char *label_text)
{
  t_menu_label	*new_label;
  TTF_Font	*label_font;
  SDL_Surface	*label_surface;
  SDL_Rect	*label_position;

  SDL_Color label_color = {116, 208, 241, 0};
  if ((new_label = malloc(sizeof(t_menu_label))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  if ((label_font = TTF_OpenFont(MENU_INFO_FONT_PATH, 20)) == NULL)
    return (my_null_error(OPEN_ERROR));
  if ((label_surface =
       TTF_RenderText_Solid(label_font, label_text, label_color)) == NULL)
    return (my_null_error(RENDER_TEXT_ERROR));
  new_label->label_font = label_font;
  new_label->label_color = label_color;
  new_label->label_text = label_text;
  new_label->label_surface = label_surface;
  if ((new_label->label_texture =
       SDL_CreateTextureFromSurface(renderer, label_surface)) == NULL)
    return (my_null_error(TEXTURE_ERROR));
  if ((label_position = get_label_info_position(new_label->label_texture)) == NULL)
    return (NULL);
  new_label->label_position = *label_position;
  return (new_label);
}
