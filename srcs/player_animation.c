#include "player_animation.h"

static t_textures_position g_textures_position[] = {
  {DIRECTION_UP, 16, 24, 128, 0, 48},
  {DIRECTION_RIGHT, 16, 24, 128, 0, 24},
  {DIRECTION_DOWN, 16, 24, 128, 0, 0},
  {DIRECTION_LEFT, 16, 24, 128, 0, 24}
};

void	set_player_sprite_position(t_player *player)
{
  if ((player->sprite->sprite_rect.x +
       g_textures_position[player->direction].width)
      < (g_textures_position[player->direction].max_width))
    player->sprite->sprite_rect.x +=
      g_textures_position[player->direction].width;
  else
    player->sprite->sprite_rect.x =
      g_textures_position[player->direction].first_sprite_x;
  player->sprite->sprite_rect.y =
    g_textures_position[player->direction].first_sprite_y;
  player->sprite->sprite_rect.w = g_textures_position[player->direction].width;
  player->sprite->sprite_rect.h = g_textures_position[player->direction].height;
}

void	set_player_sprite_properties(t_player *player)
{
  if (player->is_walking == TRUE)
    {
      if (player->sprite->animation_time.last_update >=
	  player->sprite->animation_time.refresh_time)
	{
	  set_player_sprite_position(player);
	  player->sprite->animation_time.last_update = 0;
	}
    }
  else
    {
      player->sprite->sprite_rect.x =
	g_textures_position[player->direction].first_sprite_x;
      player->sprite->sprite_rect.y =
	g_textures_position[player->direction].first_sprite_y;
      player->sprite->sprite_rect.w = g_textures_position[player->direction].width;
      player->sprite->sprite_rect.h = g_textures_position[player->direction].height;
    }
}
