#include <stdlib.h>
#include "bonus.h"
#include "parse_server_frame_utils.h"
#include "serialize.h"
#include "server_event_types.h"
#include "update_map.h"
#include "utils.h"

t_bool		parse_bonus(t_game *game, t_draw_game_stuff *dgs, char *frame,
			    char object_type)
{
  t_square	model_square;
  t_coordinates	coords;

  UNUSED(dgs);
  if (create_square_model(object_type, frame, &model_square) == FALSE)
    return (FALSE);
  coords.x = ((t_bonus*)model_square.object)->x;
  coords.y = ((t_bonus*)model_square.object)->y;
  add_object_to_map(game->map, &model_square, &coords);
  return (TRUE);
}

t_bool			parse_gone_obj(t_game *game, t_draw_game_stuff *dgs, char *frame,
					 char object_type)
{
  t_square		model_square;
  t_gone_obj_event	tmp_gone_obj;
  t_coordinates		coords;

  deserialize_gone_obj_event(frame, &tmp_gone_obj, object_type);
  if (create_square_model(object_type, frame, &model_square) == FALSE)
    return (FALSE);
  coords.x = tmp_gone_obj.pos_x;
  coords.y = tmp_gone_obj.pos_y;
  if (object_type == 9)
    {
      ((t_bomb*)model_square.object)->x = coords.x;
      ((t_bomb*)model_square.object)->y = coords.y;
    }
  add_object_to_map(game->map, &model_square, &coords);
  UNUSED(dgs);
  return (TRUE);
}

t_bool		parse_bomb_released(t_game *game, t_draw_game_stuff *dgs, char *frame,
				    char object_type)
{
  t_coordinates	coords;
  t_square	model_square;

  UNUSED(dgs);
  if (create_square_model(object_type, frame, &model_square) == FALSE)
    return (FALSE);
  coords.x = ((t_bomb*)model_square.object)->x;
  coords.y = ((t_bomb*)model_square.object)->y;
  add_object_to_map(game->map, &model_square, &coords);
  return (TRUE);
}

t_bool			parse_win_or_death(t_game *game, t_draw_game_stuff *dgs, char *frame,
					   char object_type)
{
  t_win_or_death_event	tmp_win_or_death;

  deserialize_win_or_death_event(frame, &tmp_win_or_death, object_type);
  free(game->players[tmp_win_or_death.player_id]);
  game->players[tmp_win_or_death.player_id] = NULL;
  UNUSED(dgs);
  return (TRUE);
}

t_bool		parse_moved_player(t_game *game, t_draw_game_stuff *dgs, char *frame,
				   char object_type)
{
  t_player	tmp_player;

  UNUSED(object_type);
  deserialize_moved_player_event(frame, &tmp_player);
  game->players[tmp_player.color]->x = tmp_player.x;
  game->players[tmp_player.color]->y = tmp_player.y;
  game->players[tmp_player.color]->direction = tmp_player.direction;
  dgs->players[tmp_player.color]->x = tmp_player.x;
  dgs->players[tmp_player.color]->y = tmp_player.y;
  dgs->players[tmp_player.color]->direction = tmp_player.direction;
  dgs->players[tmp_player.color]->is_walking = tmp_player.is_walking;
  return (TRUE);
}

t_bool				parse_bomb_explosion(t_game *game, t_draw_game_stuff *dgs,
						      char *frame, char object_type)
{
  int				i;
  int				x;
  int				y;
  t_bomb_explosion_event	tmp_bomb_explosion;

  UNUSED(object_type);
  deserialize_bomb_explosion_event(frame, &tmp_bomb_explosion);
  i = 0;
  while (i < tmp_bomb_explosion.positions_len)
    {
      x = tmp_bomb_explosion.positions[i].x;
      y = tmp_bomb_explosion.positions[i].y;
      game->map[y][x]->is_exploding = TRUE;
      ++i;
    }
  UNUSED(dgs);
  return (TRUE);
}
