#include <stdlib.h>
#include "game.h"
#include "errors.h"
#include "player.h"
#include "map.h"

t_game		*init_game(char *map_path, int nb_players)
{
  t_game	*game;
  int		i;

  if ((game = malloc(sizeof(t_game))) == NULL
      || (game->bombs = malloc(sizeof(t_bomb*))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  game->bombs[0] = NULL;
  game->player_id = 0;
  if ((game->map = init_map(map_path)) == NULL
      || check_map(game) == FALSE)
    {
      free_game(game);
      return (NULL);
    }
  i = 0;
  while (i < 4)
    {
      game->players[i] = (i < nb_players) ? (init_player(game, i)) : (NULL);
      ++i;
    }
  return (game);
}

t_bool	game_is_over(t_game *game)
{
  int	nb;
  int	i;

  nb = 0;
  i = 0;
  while (i < 4)
    {
      nb += (game->players[i]) ? (1) : (0);
      ++i;
    }
  return (nb <= 1);
}

/*
** Used by server only
*/
t_game		*get_game(t_game *object, t_bool init)
{
  static t_game	*game = NULL;

  if (init)
    game = object;
  return (game);
}
