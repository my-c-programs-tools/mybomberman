#include "communication.h"
#include "user_movement.h"
#include "bonus.h"

static t_axis	g_directions[] = {
  UP_AXIS,
  RGT_AXIS,
  DWN_AXIS,
  LFT_AXIS
};

static t_bool	move(t_game *game, t_axis *axis,
		     t_player *player, unsigned int elapsed)
{
  double	new_x;
  double	new_y;
  double	next_x;
  double	next_y;
  double	speed_coeff;

  speed_coeff = (2 + 0.5 * (double)player->speed) * ((double)elapsed / 1000.0);
  new_x = player->x + ((double)axis->x * speed_coeff);
  new_y = player->y + ((double)axis->y * speed_coeff);
  if (new_x < 0 || new_x >= game->map_width)
    new_x = closest(new_x, game->map_width - 1, 0);
  if (new_y < 0 || new_y >= game->map_height)
    new_y = closest(new_y, game->map_height - 1, 0);
  if (game->map[(int)new_y][(int)new_x]->type == SQUARE_BONUS &&
      apply_bonus(game, player, game->map[(int)new_y][(int)new_x]->object) == FALSE)
    return (FALSE);
  next_x = (int)player->x + axis->x;
  next_y = (int)player->y + axis->y;
  if ((int)new_x != next_x || (int)new_y != next_y
      || game->map[(int)next_y][(int)next_x]->type == SQUARE_FREE
      || game->map[(int)next_y][(int)next_x]->type == SQUARE_BONUS)
    {
      player->x = new_x;
      player->y = new_y;
      if (send_moved_player_to_client(player, MOVED_PLAYER) == FALSE)
	return (FALSE);
    }
  else
    ++player;
  return (TRUE);
}

t_bool		move_up(t_game *game, t_player *player, unsigned int elapsed)
{
  player->direction = DIRECTION_UP;
  return (move(game, &g_directions[DIRECTION_UP], player, elapsed));
}

t_bool		move_right(t_game *game, t_player *player, unsigned int elapsed)
{
  player->direction = DIRECTION_RIGHT;
  return (move(game, &g_directions[DIRECTION_RIGHT], player, elapsed));
}

t_bool		move_down(t_game *game, t_player *player, unsigned int elapsed)
{
  player->direction = DIRECTION_DOWN;
  return (move(game, &g_directions[DIRECTION_DOWN], player, elapsed));
}

t_bool		move_left(t_game *game, t_player *player, unsigned int elapsed)
{
  player->direction = DIRECTION_LEFT;
  return (move(game, &g_directions[DIRECTION_LEFT], player, elapsed));
}
