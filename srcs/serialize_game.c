#include <stdlib.h>
#include "serialize.h"
#include "errors.h"

char		*deserialize_players(char *buffer, t_game *game)
{
  int		i;
  int		nb_players;
  int		index_player;
  t_player	*player;

  i = 0;
  while (i < 4)
    {
      game->players[i] = NULL;
      ++i;
    }
  buffer = deserialize_int(buffer, &nb_players);
  i = 0;
  while (i < nb_players)
    {
      buffer = deserialize_int(buffer, &index_player);
      if ((player = malloc(sizeof(t_player))) == NULL)
	return (my_null_error(MEMORY_ERROR));
      buffer = deserialize_player(buffer, player);
      game->players[index_player] = player;
      ++i;
    }
  return (buffer);
}

char		*deserialize_map(char *buffer, t_game *game)
{
  int		map_width;
  int		map_height;
  int		x;
  int		y;
  t_square	*square;

  buffer = deserialize_int(buffer, &map_width);
  game->map_width = map_width;
  buffer = deserialize_int(buffer, &map_height);
  game->map_height = map_height;
  if ((game->map = malloc(sizeof(t_square*) * (game->map_height + 1))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  y = 0;
  while (y < map_height)
    {
      if ((game->map[y] = malloc(sizeof(t_square*) * (game->map_width + 1))) == NULL)
	return (my_null_error(MEMORY_ERROR));
      x = 0;
      while (x < map_width)
	{
	  if ((square = malloc(sizeof(t_square))) == NULL)
	    return (my_null_error(MEMORY_ERROR));
	  buffer = deserialize_square(buffer, square);
	  game->map[y][x] = square;
	  ++x;
	}
      game->map[y][x] = NULL;
      ++y;
    }
  game->map[y] = NULL;
  return (buffer);
}

int		count_players(t_player *players[4])
{
  int	i;
  int	nb_players;

  nb_players = 0;
  i = 0;
  while (i < 4)
    {
      if (players[i] != NULL)
	++nb_players;
      ++i;
    }
  return (nb_players);
}

char	*serialize_game(char *buffer, t_game *game)
{
  int	nb_players;
  int	x;
  int	y;

  buffer = serialize_int(buffer, game->player_id);
  nb_players = count_players(game->players);
  buffer = serialize_int(buffer, nb_players);
  x = 0;
  while (x < 4)
    {
      if (game->players[x] != NULL)
	{
	  buffer = serialize_int(buffer, x);
	  buffer = serialize_player(buffer, game->players[x]);
	}
      ++x;
    }
  buffer = serialize_int(buffer, game->map_width);
  buffer = serialize_int(buffer, game->map_height);
  y = 0;
  while (y < game->map_height)
    {
      x = 0;
      while (x < game->map_width)
	{
	  buffer = serialize_square(buffer, game->map[y][x]);
	  ++x;
	}
      ++y;
    }
  return (buffer);
}

t_game		*deserialize_game(char *buffer)
{
  t_game	*game;
  int		id;
  char		*old_buffer;

  if ((game = malloc(sizeof(t_game))) == NULL)
    return (my_null_error(MEMORY_ERROR));
  old_buffer = buffer;
  buffer = deserialize_int(buffer, &id);
  game->player_id = id;
  buffer = deserialize_players(buffer, game);
  buffer = deserialize_map(buffer, game);
  free(old_buffer);
  return (game);
}
