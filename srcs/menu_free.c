#include <stdlib.h>
#include "menu_free.h"

void	free_editable_fields(t_menu_content *menu_content)
{
  menu_content->is_ip_field_selected = FALSE;
  free(menu_content->ip_field);
  menu_content->is_nb_player_selected = FALSE;
  free(menu_content->nb_player_field);
}
