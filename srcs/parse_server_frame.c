#include <stdio.h>
#include "serialize.h"
#include "parse_server_frame.h"
#include "parse_server_frame_utils.h"

/*
** SI SWITCH CASE NON FONCTIONNEL SOUS WINDOWS, UTILISER SE TABLEAU DE PTR SUR FONCTION

static t_recv_event_action recv_event_action[] = {
  parse_bonus,
  parse_bonus,
  parse_bonus,
  parse_bonus,
  parse_bonus,
  parse_bonus,
  parse_bonus,
  parse_bonus,
  parse_gone_obj,
  parse_bomb_released,
  parse_win_or_death,
  parse_win_or_death,
  parse_moved_player,
  parse_bomb_explosion
};
*/

t_bool	parse_server_frame(t_game *game, t_draw_game_stuff *dgs, char *frame)
{
  char	object_type;

  
  frame = deserialize_char(frame, &object_type);
/*
** SI SWITCH CASE NON FONCTIONNEL SOUS WINDOWS, UTILISER SE TABLEAU DE PTR SUR FONCTION

  if (recv_event_action[(int) object_type](game, dgs, frame, object_type) == FALSE)
  return (FALSE);
*/
  switch (object_type)
    {
    case 0 ... 7:
      if (parse_bonus(game, dgs, frame, object_type) == FALSE)
	return (FALSE);
      break;
    case 8:
      if (parse_gone_obj(game, dgs, frame, object_type) == FALSE)
	return (FALSE);
      break;
    case 9:
       if (parse_bomb_released(game, dgs, frame, object_type) == FALSE)
	return (FALSE);
      break;
    case 10 ... 11:
      if (parse_win_or_death(game, dgs, frame, object_type) == FALSE)
	return (FALSE);
      break;
    case 12:
      if (parse_moved_player(game, dgs, frame, object_type) == FALSE)
	return (FALSE);
      break;
    case 13:
      if (parse_bomb_explosion(game, dgs, frame, object_type) == FALSE)
	return (FALSE);
      break;
    }
  return (TRUE);
}

