#include <stdlib.h>
#include "communication.h"
#include "errors.h"
#include "bomb_timer.h"
#include "bomb_explode.h"
#include "store_explosion_coords.h"

static int	g_bomb_durations[] = {
  2500,
  2500,
  3500,
  20000
};

static t_bool	delete_bomb(t_game *game, int i)
{
  int		x;
  int		y;

  x = game->bombs[i]->x;
  y = game->bombs[i]->y;
  game->map[y][x]->type = SQUARE_FREE;
  game->map[y][x]->object = NULL;
  if (send_gone_obj_to_client(GONE_OBJECT, x, y) == FALSE)
    return (FALSE);
  while (game->bombs[i])
    {
      game->bombs[i] = game->bombs[i + 1];
      ++i;
    }
  if ((game->bombs = realloc(game->bombs, sizeof(t_bomb*) * i)) == NULL)
    return (my_bool_error(MEMORY_ERROR));
  return (TRUE);
}

static t_bool	trigger_bomb(t_game *game, int i)
{
  t_bomb	*bomb;

  bomb = game->bombs[i];
  if (delete_bomb(game, i) == FALSE
      || explode(bomb) == FALSE)
    return (FALSE);
  send_bomb_explosion_to_client(EXPLOSION);
  free_explosion_coords();
  get_explosion_coords(NULL, TRUE);
  return (TRUE);
}

t_bool	        remote_trigger(t_bomb *bomb)
{
  t_game	*game;
  int		i;

  game = get_game(NULL, FALSE);
  i = 0;
  while (game->bombs[i] && game->bombs[i] != bomb)
    ++i;
  if (game->bombs[i] && (delete_bomb(game, i) == FALSE
			 || explode(bomb) == FALSE))
    return (FALSE);
  return (TRUE);
}

t_bool		refresh_bombs(unsigned int elapsed_time)
{
  t_game	*game;
  int		i;

  game = get_game(NULL, FALSE);
  i = 0;
  while (game->bombs[i])
    {
      game->bombs[i]->elapsed_time += elapsed_time;
      if (game->bombs[i]->elapsed_time >= g_bomb_durations[game->bombs[i]->type])
	{
	  if (trigger_bomb(game, i) == FALSE)
	    return (FALSE);
	}
      else
	++i;
    }
  return (TRUE);
}
