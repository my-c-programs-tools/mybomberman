#include "configuration.h"
#include "player_animation.h"
#include "players_draw.h"
#include "player.h"

void		get_player_sprite_position(t_player *player,
					   int square_width_size,
					   int square_height_size)
{
  SDL_Rect	*rect;

  rect = &player->sprite->sprite_renderer_position;
  rect->x = (int)(player->x * square_width_size);
  rect->y = (int)(player->y * square_height_size);
  rect->w = (int)square_width_size / 2;
  rect->h = (int)((square_height_size * 5) / 6);
  rect->x -= rect->w / 2;
  rect->y -= (5 * rect->h) / 6;
}

t_bool		draw_player(SDL_Renderer *renderer, t_player *player,
		    int square_width_size, int square_height_size)
{
  get_player_sprite_position(player, square_width_size, square_height_size);
  if (player->direction != DIRECTION_LEFT)
    {
      if (SDL_RenderCopy(renderer, player->sprite->sprite_texture,
			 &player->sprite->sprite_rect,
			 &player->sprite->sprite_renderer_position) != 0)
	return (FALSE);
    }
  else
    {
      SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL;
      if (SDL_RenderCopyEx(renderer, player->sprite->sprite_texture,
			   &player->sprite->sprite_rect,
			   &player->sprite->sprite_renderer_position,
			   0, NULL, flip) != 0)
	return (FALSE);
    }
  return (TRUE);
}

t_bool	draw_players(t_draw_game_stuff *dgs, int elapsed, int square_width_size,
		     int square_height_size)
{
  int	i;

  i = 0;
  while (i < 4)
    {
      if (dgs->players[i])
	{
	  dgs->players[i]->sprite->animation_time.last_update += elapsed;
	  set_player_sprite_properties(dgs->players[i]);
	  if (draw_player(dgs->renderer, dgs->players[i],
			  square_width_size, square_height_size) == FALSE)
	    return (FALSE);
	}
      i++;
    }
  return (TRUE);
}
