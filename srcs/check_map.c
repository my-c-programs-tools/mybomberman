#include <stdlib.h>
#include "game.h"
#include "utils.h"
#include "errors.h"

static int	get_line_size(t_square **line)
{
  int		line_length;

  line_length = 0;
  while (line[line_length] != NULL)
    ++line_length;
  return (line_length);
}

t_bool		check_map(t_game *game)
{
  int		map_height;
  int		map_width_p;
  int		map_width;
  t_square	***map;

  map = game->map;
  map_width_p = get_line_size(map[0]);
  map_height = 1;
  while (map[map_height] != NULL)
    {
      map_width = get_line_size(map[map_height]);
      if (map_width_p != map_width)
	return (my_bool_error(MAP_ERROR));
      map_width_p = map_width;
      ++map_height;
    }
  game->map_width = map_width;
  game->map_height = map_height;
  return (TRUE);
}
