#include <stdlib.h>
#include "serialize.h"
#include "errors.h"

char	*serialize_bomb(char *buffer, t_bomb *bomb)
{
  buffer = serialize_int(buffer, bomb->x);
  buffer = serialize_int(buffer, bomb->y);
  buffer = serialize_int(buffer, bomb->type);
  return (buffer);
}

char	*deserialize_bomb(char *buffer, t_bomb *bomb)
{
  int	x;
  int	y;
  int	type;

  buffer = deserialize_int(buffer, &x);
  bomb->x = x;
  buffer = deserialize_int(buffer, &y);
  bomb->y = y;
  buffer = deserialize_int(buffer, &type);
  bomb->type = (t_bomb_type)type;
  return (buffer);
}

char	*serialize_bonus(char *buffer, t_bonus *bonus)
{
  buffer = serialize_int(buffer, bonus->x);
  buffer = serialize_int(buffer, bonus->y);
  buffer = serialize_int(buffer, bonus->type);
  buffer = serialize_int(buffer, bonus->malus);
  return (buffer);
}

char	*deserialize_bonus(char *buffer, t_bonus *bonus)
{
  int	x;
  int	y;
  int	type;
  int	malus;

  buffer = deserialize_int(buffer, &x);
  bonus->x = x;
  buffer = deserialize_int(buffer, &y);
  bonus->y = y;
  buffer = deserialize_int(buffer, &type);
  bonus->type = (t_bomb_type)type;
  buffer = deserialize_int(buffer, &malus);
  bonus->malus = (t_bool)malus;
  return (buffer);
}

char	*serialize_square(char *buffer, t_square *square)
{
  buffer = serialize_int(buffer, square->type);
  buffer = serialize_int(buffer, (int)square->is_exploding);
  if (square->type == SQUARE_BOMB)
    buffer = serialize_bomb(buffer, (t_bomb*)square->object);
  else if (square->type == SQUARE_BONUS)
    buffer = serialize_bonus(buffer, (t_bonus*)square->object);
  return (buffer);
}

char		*deserialize_square(char *buffer, t_square *square)
{
  int		type;
  t_bool	exploding;
  void		*object;

  buffer = deserialize_int(buffer, &type);
  square->type = (t_square_type)type;
  buffer = deserialize_int(buffer, (int*)(&exploding));
  square->is_exploding = (t_square_type)exploding;
  if (type == SQUARE_BOMB)
    {
      if ((object = malloc(sizeof(t_bomb))) == NULL)
	return (my_null_error(object));
      buffer = deserialize_bomb(buffer, (t_bomb*)object);
      square->object = object;
    }
  else if (type == SQUARE_BONUS)
    {
      if ((object = malloc(sizeof(t_bonus))) == NULL)
	return (my_null_error(object));
      buffer = deserialize_bonus(buffer, (t_bonus*)object);
      square->object = object;
    }
  return (buffer);
}
