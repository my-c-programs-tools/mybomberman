#ifdef _WIN32
# include <Winsock2.h>
#endif

#include <time.h>
#include <stdlib.h>
#include "timer.h"
#include "menu_window.h"
#include "bonus.h"
#include "bonus_random.h"
#include "utils.h"
#include "errors.h"

int	main(int argc, char **argv)
{
  UNUSED(argc);
  UNUSED(argv);

  /* Init Winsock DLL for use */
  #ifdef _WIN32
    int ret;
    WSADATA wsaData;
    if ((ret = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0)
      return (my_int_error(WSA_STRT_ERROR, -1));
  #endif

  srand(time(NULL));
  init_bonus_percentages();
  init_bomb_percentages();
  if (init_window() == FALSE)
    return (-1);

  /* Declare we are done with Winsock DLL */
  #ifdef _WIN32
    WSACleanup();
  #endif

  return (0);
}
