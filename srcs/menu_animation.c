#include <stdlib.h>
#include "configuration.h"
#include "menu_animation.h"

void	init_animation_properties(t_animated_sprite *sprite, int pos_x, int pos_y)
{
  sprite->sprite_renderer_position.x = pos_x;
  sprite->sprite_renderer_position.y = pos_y;
  sprite->sprite_renderer_position.w = sprite->sprite_rect.w;
  sprite->sprite_renderer_position.h = sprite->sprite_rect.h;
}

void	change_flying_bomber_direction(t_animated_sprite *menu_flying_bomber,
				       unsigned int elapsed)
{  
  if (menu_flying_bomber->sprite_renderer_position.x >= WINDOW_WIDTH ||
      menu_flying_bomber->sprite_renderer_position.x <= -50)
    {
      if (menu_flying_bomber->is_visible == TRUE &&
	  ((menu_flying_bomber->sprite_renderer_position.x >=
	    WINDOW_WIDTH && menu_flying_bomber->direction == DIRECTION_RIGHT) ||
	   (menu_flying_bomber->sprite_renderer_position.x <= -50 &&
	    menu_flying_bomber->direction == DIRECTION_LEFT)))
	menu_flying_bomber->is_visible = FALSE;
      if (menu_flying_bomber->is_visible == FALSE)
	menu_flying_bomber->animation_time.last_update += elapsed;
      if (menu_flying_bomber->animation_time.last_update >=
	  menu_flying_bomber->animation_time.refresh_time)
	{
	  menu_flying_bomber->animation_time.last_update = 0;
	  if (menu_flying_bomber->direction == DIRECTION_RIGHT)
	    menu_flying_bomber->direction = DIRECTION_LEFT;
	  else
	    menu_flying_bomber->direction = DIRECTION_RIGHT;
	  menu_flying_bomber->is_visible = TRUE;
	}
    }
}
  
void		change_flying_bomber_position(t_animated_sprite *menu_flying_bomber,
					      unsigned int elapsed)
{
  int		rand_should_move;

  rand_should_move = rand() % 4;
  if (menu_flying_bomber->direction == DIRECTION_RIGHT)
    menu_flying_bomber->sprite_renderer_position.x +=
      (elapsed / 10) * menu_flying_bomber->speed;
  else
    menu_flying_bomber->sprite_renderer_position.x -=
      (elapsed / 10) * menu_flying_bomber->speed;
  change_flying_bomber_direction(menu_flying_bomber, elapsed);
  if (menu_flying_bomber->spe_act == 0 && rand_should_move == 0)
    menu_flying_bomber->sprite_renderer_position.y += 1;
  else if (menu_flying_bomber->spe_act == 1 && rand_should_move == 0)
    menu_flying_bomber->sprite_renderer_position.y -= 1;
  if (menu_flying_bomber->sprite_renderer_position.y >= 30 &&
      menu_flying_bomber->spe_act == 0)
    menu_flying_bomber->spe_act = 1;
  else if (menu_flying_bomber->sprite_renderer_position.y <= 10 &&
	   menu_flying_bomber->spe_act == 1)
    menu_flying_bomber->spe_act = 0;
}

void		move_pirate(t_animated_sprite *menu_pirate, unsigned int elapsed)
{
  int		rand_should_stop;

  if (menu_pirate->sprite_rect.x > 127 && menu_pirate->spe_act == 0)
    menu_pirate->sprite_rect.x = 0;
  if ((rand_should_stop = rand() % 100) == 1)
    menu_pirate->spe_act = 1;
  if (menu_pirate->spe_act == 1)
    {
      menu_pirate->sprite_rect.x = 144;
      if ((rand_should_stop = rand() % 40) == 1) {
        menu_pirate->spe_act = 0;
      }
    }
  else if (menu_pirate->spe_act == 0 && menu_pirate->direction == DIRECTION_RIGHT)
    menu_pirate->sprite_renderer_position.x += (elapsed / 10) * menu_pirate->speed;
  else if (menu_pirate->spe_act == 0 && menu_pirate->direction == DIRECTION_LEFT)
    menu_pirate->sprite_renderer_position.x -= (elapsed / 10) * menu_pirate->speed;
  if (menu_pirate->sprite_renderer_position.x
      >= (WINDOW_WIDTH - menu_pirate->sprite_rect.w))
    menu_pirate->direction = DIRECTION_LEFT;
  else if (menu_pirate->sprite_renderer_position.x <= 0)
    menu_pirate->direction = DIRECTION_RIGHT;
}

void		animate_pirate(t_animated_sprite *menu_pirate,
			       unsigned int elapsed)
{
  menu_pirate->animation_time.last_update += elapsed;
  if (menu_pirate->animation_time.last_update >=
      menu_pirate->animation_time.refresh_time)
    {
      menu_pirate->sprite_rect.x += 16;
      menu_pirate->animation_time.last_update = 0;
    }
  move_pirate(menu_pirate, elapsed);
}
