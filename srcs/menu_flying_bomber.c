#include <stdlib.h>
#include "configuration.h"
#include "errors.h"
#include "menu_flying_bomber.h"
#include "menu_animation.h"

t_bool	init_flying_bomber_bomb(t_menu_content *menu_content)
{
  init_animation_properties(menu_content->bomb,
			    menu_content->menu_flying_bomber->
			    sprite_renderer_position.x + 5,
			    menu_content->menu_flying_bomber->
			    sprite_renderer_position.y + 21);
  if ((menu_content->bomb_destination = malloc(sizeof(t_coordinates)))
      == NULL)
    return (my_bool_error(MEMORY_ERROR));
  menu_content->bomb_destination->x =
    menu_content->menu_flying_bomber->sprite_renderer_position.x;
  menu_content->bomb_destination->y = (rand() % 100) +
    ((WINDOW_HEIGHT / 3) * 2); 
  menu_content->bomb->is_visible = TRUE;
  return (TRUE);
}

void	handle_explode_animation(t_menu_content *menu_content)
{
  if (menu_content->bomb->sprite_rect.x < 51)
    menu_content->bomb->sprite_rect.x += 17;
  else if (menu_content->bomb->sprite_rect.x == 102)
    {
      if (menu_content->bomb->sprite_rect.y == 51)
	{
	  menu_content->bomb->is_visible = FALSE;
	  menu_content->bomb->animation_time.refresh_time = 1;
	  free(menu_content->bomb_destination);
	}
      menu_content->bomb->sprite_rect.y -= 17;
    }
  else
    {
      menu_content->bomb->sprite_rect.x = 102;
      menu_content->bomb->sprite_rect.y = 85;
      menu_content->bomb->animation_time.refresh_time = 50;
    }
}

void	menu_drop_bomb(t_menu_content *menu_content, unsigned int elapsed)
{
  menu_content->bomb->animation_time.last_update += elapsed;
  if (menu_content->bomb->animation_time.last_update >=
      menu_content->bomb->animation_time.refresh_time)
    {
      menu_content->bomb->animation_time.last_update = 0;
      if (menu_content->bomb->sprite_renderer_position.y <
	  menu_content->bomb_destination->y)
	menu_content->bomb->sprite_renderer_position.y += (elapsed / 10)
	  * menu_content->bomb->speed;
      else if (menu_content->bomb->sprite_renderer_position.y >=
	       menu_content->bomb_destination->y)
	{
	  menu_content->bomb->animation_time.refresh_time = 200;
	  handle_explode_animation(menu_content);
	}
    }
}

t_bool	should_drop_bomb(t_menu_content *menu_content, unsigned int elapsed)
{
  int	rand_should_drop_bomb;

  rand_should_drop_bomb = rand() % 100;
  if (menu_content->bomb->is_visible == TRUE)
    menu_drop_bomb(menu_content, elapsed);
  else if (rand_should_drop_bomb == 0 && menu_content->bomb->is_visible == FALSE)
    {
      if (menu_content->menu_flying_bomber->sprite_renderer_position.x >= 0 &&
	  menu_content->menu_flying_bomber->sprite_renderer_position.x <=
	  (WINDOW_WIDTH - menu_content->menu_flying_bomber->sprite_renderer_position.w) &&
	  init_flying_bomber_bomb(menu_content) == FALSE)
	return (FALSE);
    }
  if (menu_content->bomb->is_visible == FALSE && menu_content->bomb->sprite_rect.x != 0)
    {
      menu_content->bomb->sprite_rect.x = 0;
      menu_content->bomb->sprite_rect.y = 0;
    }
  return (TRUE);
}
