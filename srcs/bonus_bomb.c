#include <stdlib.h>
#include "bonus.h"
#include "bonus_bomb.h"

void		bonus_bnuke(t_player *player, t_bonus *bonus)
{
  UNUSED(bonus);
  player->bomb_type = BOMB_NUKE;
}

void		bonus_bcross(t_player *player, t_bonus *bonus)
{
  UNUSED(bonus);
  player->bomb_type = BOMB_CROSS;
}

void		bonus_bstar(t_player *player, t_bonus *bonus)
{
  UNUSED(bonus);
  player->bomb_type = BOMB_STAR;
}

void		bonus_bdiagonal(t_player *player, t_bonus *bonus)
{
  UNUSED(bonus);
  player->bomb_type = BOMB_DIAGONAL;
}
