#ifndef		CONFIGURATION_H_
# define	CONFIGURATION_H_

# include	"bomb.h"
# include	"utils.h"

/*
** Here are define all the macros used for basic configuration
*/

/*
** Graphic configurations
*/
# define	WINDOW_WIDTH		(990)
# define	WINDOW_HEIGHT		(845)
# define    FPS			        (60)

/*
** Default player stats
*/
# define	DFLT_P_BMB_TYPE		(BOMB_CROSS)
# define	DFLT_P_BMB_RNGE		(2)
# define	DFLT_P_BMB_LMIT		(3)
# define	DFLT_P_SPEED		(1)
# define	DFLT_P_NAME		    ("Player")

/*
** Main bonus chance spawn in %
*/
# define	BONUS_SPWN_CHNC		(70)
# define	MALUS_CHNC		    (15.0)

/*
** Bonus types spawn chances in proportionnal values
*/
# define	BNUKE_SPWN_CHNC		(75)
# define	BCROSS_SPWN_CHNC	(175)
# define	BDIAGONAL_SPWN_CHNC	(100)
# define	BSTAR_SPWN_CHNC		(250)
# define	SPEED_SPWN_CHNC		(500)
# define	RANGE_SPWN_CHNC		(500)
# define	BRANDOM_SPWN_CHNC	(500)
# define	BOMB_SPWN_CHNC		(650)

/*
** Default explosion behavior
**
** TCH_STOP : explosion stops at the object
** DESTROYABLE : object is destroyable
**
** Bombs implements STOP and DESTROYABLE
** Bonus implements STOP and DESTROYABLE
** Player implements STOP
** Destructible Walls implements STOP
*/
# define	BOMB_TCH_STOP		(TRUE)
# define	BOMB_DESTROYABLE	(TRUE)
# define	BONUS_TCH_STOP		(FALSE)
# define	BONUS_DESTROYABLE	(FALSE)
# define	PLAYER_TCH_STOP		(FALSE)
# define    WALL_TCH_STOP		(TRUE)

#endif		/* !CONFIGURATION_H_ */
