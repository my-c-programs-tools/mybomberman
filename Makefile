SRC_DIR		=	srcs/

SRCS		=	$(SRC_DIR)my_strcpy.c \
			$(SRC_DIR)my_strncpy.c \
			$(SRC_DIR)my_strcmp.c \
			$(SRC_DIR)my_strlen.c \
			$(SRC_DIR)my_strcat.c \
			$(SRC_DIR)my_getnbr.c \
			$(SRC_DIR)my_remove_n_first_char.c \
			$(SRC_DIR)free_menu_tools.c \
			$(SRC_DIR)errors.c \
			$(SRC_DIR)main.c \
			$(SRC_DIR)menu.c \
			$(SRC_DIR)menu_init.c \
			$(SRC_DIR)menu_tools.c \
			$(SRC_DIR)menu_draw.c \
			$(SRC_DIR)draw_window.c \
			$(SRC_DIR)menu_animation.c \
			$(SRC_DIR)timer.c \
			$(SRC_DIR)menu_window.c \
			$(SRC_DIR)network_init.c \
			$(SRC_DIR)bonus_bomb.c \
			$(SRC_DIR)bonus_player.c \
			$(SRC_DIR)bonus.c \
			$(SRC_DIR)menu_flying_bomber.c \
			$(SRC_DIR)menu_event.c \
			$(SRC_DIR)menu_music.c \
			$(SRC_DIR)menu_informations.c \
			$(SRC_DIR)menu_play.c \
			$(SRC_DIR)menu_select_label.c \
			$(SRC_DIR)editable_label.c \
			$(SRC_DIR)menu_free.c \
			$(SRC_DIR)game.c \
			$(SRC_DIR)free_game.c \
			$(SRC_DIR)map_draw.c \
			$(SRC_DIR)game_draw.c \
			$(SRC_DIR)graphic_map.c \
			$(SRC_DIR)graphic_explosion_tools.c \
			$(SRC_DIR)texture_handler.c \
			$(SRC_DIR)game_loop.c \
			$(SRC_DIR)server_loop.c \
			$(SRC_DIR)explosion_draw.c \
			$(SRC_DIR)players_draw.c \
			$(SRC_DIR)player_animation.c \
			$(SRC_DIR)map.c \
			$(SRC_DIR)destroy.c \
			$(SRC_DIR)destroy_functions.c \
			$(SRC_DIR)bomb_explode.c \
			$(SRC_DIR)bomb_timer.c \
			$(SRC_DIR)bonus_random.c \
			$(SRC_DIR)percentages.c \
			$(SRC_DIR)check_map.c \
			$(SRC_DIR)end_game_display.c \
			$(SRC_DIR)user_action.c \
			$(SRC_DIR)user_movement.c \
			$(SRC_DIR)drop_bomb.c \
			$(SRC_DIR)closest.c \
			$(SRC_DIR)menu_select_label_tools.c \
			$(SRC_DIR)init_game_graphics.c \
			$(SRC_DIR)player.c \
			$(SRC_DIR)free_draw_game_stuff.c \
			$(SRC_DIR)serialize_game.c \
			$(SRC_DIR)serialize_map_elements.c \
			$(SRC_DIR)serialize_player.c \
			$(SRC_DIR)serialize_utils.c \
			$(SRC_DIR)serialize_tools.c \
			$(SRC_DIR)serialize_server_event.c \
			$(SRC_DIR)deserialize_server_event.c \
			$(SRC_DIR)communication.c \
			$(SRC_DIR)send_to_client.c \
			$(SRC_DIR)parse_server_frame.c \
			$(SRC_DIR)parse_server_frame_utils.c \
			$(SRC_DIR)update_map.c \
			$(SRC_DIR)store_explosion_coords.c

OBJS		=	$(SRCS:.c=.o)

NAME		=	bomberman

CC		=	gcc -o $(NAME)

RM		=	rm -f

ifeq ($(OS),Windows_NT)
	LIBS	=	-lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lpthread -lws2_32
else
	LIBS	=	-lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lpthread
endif

INCLUDES	=	$(LIBS) -Iincludes -I.

CFLAGS		=	-W -Wall -Werror $(INCLUDES)

all:			$(NAME)

$(NAME):		$(OBJS)
			$(CC) $(OBJS) $(INCLUDES)

clean:
			$(RM) $(OBJS)

fclean:			clean
			$(RM) $(NAME)

re:			fclean all

.PHONY:			all clean fclean re
